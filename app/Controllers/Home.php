<?php namespace App\Controllers;

class Home extends BaseController
{
	public function index()
	{
        $data['content'] = 'klixaja/layouts/homepage/body';
        $data['footer_script'] = 'klixaja/layouts/homepage/footscript';
        $data['header_script'] = 'klixaja/layouts/homepage/headscript';


        echo view('klixaja/main/head', $data);
        echo view('klixaja/main/body', $data);
                echo view('klixaja/main/chat', $data);

        echo view('klixaja/main/foot', $data);


	}

	//--------------------------------------------------------------------

}
