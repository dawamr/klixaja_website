<?php namespace App\Controllers;
class ChatMessage extends BaseController
{
	public function index()
	{
        $data['content'] = 'klixaja/layouts/chatmessagepage/body';
        $data['footer_script'] = 'klixaja/layouts/chatmessagepage/footscript';
        $data['header_script'] = 'klixaja/layouts/chatmessagepage/headscript';

        echo view('klixaja/main/head', $data);
        echo view('klixaja/main/body', $data);
        echo view('klixaja/main/foot', $data);

	}

	
}