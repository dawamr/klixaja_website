<?php namespace App\Controllers;

class Shipping extends BaseController
{
	public function index()
	{
        $data['content'] = 'klixaja/layouts/shippingpage/body';
        $data['footer_script'] = 'klixaja/layouts/shippingpage/footscript';
        $data['header_script'] = 'klixaja/layouts/shippingpage/headscript';

        echo view('klixaja/main/head', $data);
        echo view('klixaja/main/navbar', $data);
        echo view('klixaja/main/body', $data);
                echo view('klixaja/main/chat', $data);

        echo view('klixaja/main/foot', $data);
	}

	//--------------------------------------------------------------------

}
