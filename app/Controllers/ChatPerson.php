<?php namespace App\Controllers;
class ChatPerson extends BaseController
{
	public function index()
	{
        $data['content'] = 'klixaja/layouts/chatpersonpage/body';
        $data['footer_script'] = 'klixaja/layouts/chatpersonpage/footscript';
        $data['header_script'] = 'klixaja/layouts/chatpersonpage/headscript';

        echo view('klixaja/main/head', $data);
        echo view('klixaja/main/body', $data);
        echo view('klixaja/main/foot', $data);

	}

	
}