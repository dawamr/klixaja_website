<?php namespace App\Controllers;
class Cart extends BaseController
{
	public function index()
	{
        $data['content'] = 'klixaja/layouts/cartpage/body';
        $data['footer_script'] = 'klixaja/layouts/cartpage/footscript';
        $data['header_script'] = 'klixaja/layouts/cartpage/headscript';

        echo view('klixaja/main/head', $data);
        echo view('klixaja/main/navbar', $data);
        echo view('klixaja/main/body', $data);
        echo view('klixaja/main/chat', $data);
        echo view('klixaja/main/foot', $data);

	}

	
}