<div class="content">
<header class="header_navbar">
    <div class="navbar_list">
        <div class="navbar_top">
            <div class="navbar_top_row1">
                <a href="#" class="navbar_top_row1_link">Jual</a>
                <a href="#" class="navbar_top_row1_link">Download</a>
                <div class="navbar_top_row1_follow_us">
                    <h4 style="margin-top:5px;" class="navbar_top_row1_follow_us_title">Ikuti Kami</h4>
                    <a href="#" class="navbar_top_row1_follow_us_link">
                        <i class="ig_icon icon navbar_top_icon"></i>
                    </a>
                    <a href="#" class="navbar_top_row1_follow_us_link">
                        <i class="fb_icon icon navbar_top_icon"></i>
                    </a>
                    <!-- <a href="#" class="navbar_top_row1_follow_us_link">
                        <i class="twitter_icon icon navbar_top_icon"></i>
                    </a>
                    <a href="#" class="navbar_top_row1_follow_us_link">
                        <i class="line_icon icon navbar_top_icon"></i>
                    </a> -->
                    <a href="#" class="navbar_top_row1_follow_us_link">
                        <i class="wa_icon icon navbar_top_icon"></i>
                    </a>
                </div>
            </div>
            <div class="navbar_top_row3">
                <a href="#" class="navbar_top_row3_link">
                    <i class="help_icon icon navbar_top_icon"></i>
                    <span>Bantuan</span>
                </a>
            </div>
        </div>
        <!-- end navbar_top -->
        <div class="navbar_bottom1">
            <a href="#" class="nav_icon">
                <svg width="30" height="25">
                    <path d="M0,5 30,5" stroke="#fff" stroke-width="5" />
                    <path d="M0,14 30,14" stroke="#fff" stroke-width="5" />
                    <path d="M0,23 30,23" stroke="#fff" stroke-width="5" />
                </svg>
            </a>
            <div class="logo1">
                <a href="#" class="logo_link">
                    <div class="logo_image1" style="display: inline-block;"></div>
                </a>    
                 
            </div>
              <div id="dropdown " class="dropdown1">
                    <img clas="image-filter" src="<?php echo base_url('assets/klixaja/images/icons/filter_iconn.svg')?>" style="width: 30px;"></img>
                <div class="dropdown-content">
                <a href="#">Makanan & Minuman X-press</a>
                <a href="#">Produk Pre Order</a>
                <a href="#">Produk Terbaru</a>
                <a href="#">Produk Terlaris</a>
                </div>
            </div> 
            <div class="search_bar">
                <input type="text" id="search" placeholder="Apa yang ingin anda cari?" onclick="this.placeholder=''"
                       onblur="this.placeholder='Apa yang ingin anda cari?'" class="search_field" />
                <div class="search_icon_box">
                    <i class="search_icon icon"></i>
                </div>
            </div>
            <div class="cart" onclick="window.location.href='Cart';">
                <i class="cart_icon icon"></i>
                <div class="cart_quantity">6</div>
                <div class="cart_isosceles" style="display:none"></div>
                <div class="cart_content" style="display:none">
                    <!-- <p class="cart_no_product">Tidak ada produk.</p> -->
                    <?php for ($i = 0; $i < 3; $i++) { ?>
                        <div class="cart_item">
                            <img src="<?php echo base_url('assets/klixaja/images/item1.jpg'); ?>" class="cart_item_image">
                            <p class="cart_item_title">
                                <?php echo substr("Laborum ex fugiat cillum mollit tempor laborum excepteur do adipisicing voluptate.", 0, 50). "..."; ?>
                            </p>
                            <p class="cart_item_price"><?php echo substr("Rp150.000.000.000", 0, 13). "..."; ?></p>
                            <a href="#" class="cart_item_delete"></a>
                        </div>
                    <?php } ?>
                    <a href="cart" class="cart_button">Lihat Keranjang</a>
                </div>
            </div>
            <div class="notifications1">
                <i class="bell_icon icon"></i>
            </div>
            <div class="user dropdownnn">
 <i class="user_icon icon " onclick="myFunction()"></i>
 <div id="myDropdown" class="dropdown-contenttt">
     <div style="height:75px;display:flex;align-items:center;margin-left:20px;cursor:pointer">
                 <img style="width:50px;height:50px;border-radius:50%;margin-right:10px" src="assets/klixaja/images/item3.jpg" alt="">
                 <p style="font-weight:bold;font-size:18px">Indo Shop</p>
                </div>
            <hr  style="margin-bottom:-3px;    border-top: 1px solid rgba(0, 0, 0, 0.1);">
    <a class="dropdown-item" style="height:40px;font-size:16px;display:flex;align-items:center" href="#">Pesanan Saya</a>
        <a class="dropdown-item" style="height:40px;font-size:16px;display:flex;align-items:center" href="#">History Pembelian</a>
                <a class="dropdown-item" style="height:40px;font-size:16px;display:flex;align-items:center" href="#">TopUp Saldo</a>
    <a class="dropdown-item" style="height:40px;font-size:16px;display:flex;align-items:center" href="#">Registrasi Mitra</a>
    <a class="dropdown-item" style="height:40px;font-size:16px;display:flex;align-items:center" href="#">Edit Profile</a>
    <a class="dropdown-item" style="height:40px;font-size:16px;display:flex;align-items:center" href="#">Pengaturan</a>
    <a class="dropdown-item" style="height:40px;font-size:16px;display:flex;align-items:center" href="#">Help Center</a>
        <a class="dropdown-item" style="height:40px;font-size:16px;display:flex;align-items:center" href="#">Logout</a>

  </div>
 
                 
            </div>
        </div>
        <!-- end navbar_bottom -->
    </div>
    <!-- end navbar_list -->
</header>
   <!-- <div class="home_banners "style="margin-left:40px;margin-top:120px;">
    <div class=" "> -->
                    <!-- <div class="child nav_arrow carousel_arrow left">&#10094;</div>
                    <div class="child nav_arrow carousel_arrow right">&#10095;</div> -->
                    <!-- <div class="carousel sliderrr" style="width:900px">
                       <img src="<?php echo base_url('assets/klixaja/images/banner1.jpeg'); ?>"> 
                <img src="<?php echo base_url('assets/klixaja/images/banner1.jpeg'); ?>" > 
                <img src="<?php echo base_url('assets/klixaja/images/banner1.jpeg'); ?>" > 
                    </div>
                </div>
                <div class="right_banner">
                     <img class="right_banner_img" src="<?php echo base_url('assets/klixaja/images/banner1.jpeg'); ?>" style="height:150px;"  > 
                <img class="right_banner_img" src="<?php echo base_url('assets/klixaja/images/banner2.jpeg'); ?>" style="height:150px;margin-top:-30px;"> 
                        
                </div> -->
            <!-- <div class="child nav_arrow carousel_arrow left">&#10094;</div>
            <div class="child nav_arrow carousel_arrow right">&#10095;</div> -->
    <!-- </div> -->


     <div class="home_banners padding_box_2wh">
                <div class="parent left_banner">
                    <div class="child nav_arrow carousel_arrow left">&#10094;</div>
                    <div class="child nav_arrow carousel_arrow right">&#10095;</div>
                    <div class="carousel">
                        <div class="carousel_img active" style="background-image:url(<?php echo base_url('assets/klixaja/images/banner1.jpeg') ?>); left: 0;"
                        pos="0"></div>
                        <div class="carousel_img" style="background-image:url(<?php echo base_url('assets/klixaja/images/banner2.jpeg') ?>); left: 100%" 
                        pos="1"></div>
                        <div class="carousel_img" style="background-image:url(<?php echo base_url('assets/klixaja/images/banner3.jpeg') ?>); left: 200%"
                        pos="2"></div>
                    </div>
                </div>
                <div class="right_banner">
                    <div class="right_banner_img" style="background-image:url(<?php echo base_url('assets/klixaja/images/banner5.jpg') ?>)" ></div>
                    <div class="right_banner_img" style="background-image:url(<?php echo base_url('assets/klixaja/images/banner6.jpg') ?>)" ></div>
                </div>
            </div>
<section class="body">
 
    <div id="category" class="parent sized_box col_100 row_10_90 padding_box_2wh row_gap_10">
        <h2>Kategori</h2>
        <a href="">
        <div class="child nav_arrow slider_arrow left left_1_vw circular"><i class="icon left_icon left_icon1"></i></div>
        <!-- update class left_icon jadi right_icon -->
        <div class="child nav_arrow slider_arrow right right_1_vw circular"><i class="icon right_icon right_icon1"></i></div>
        <!-- update -->
        <div class="slider_box parent sized_box col_100" style="height: 240px">
            <?php
            $translateX = 0;
            $pos = 0;
            for ($i = 0; $i < 30; $i++) {
                $rand = rand(1,4);
                if ($i !== 0 && $i % 2 == 0) {
                    $translateX += 100;
                    $pos++;
                }
                ?>
                <div class="decoration_box_1 child sized_box" style="transform:translateX(<?php echo $translateX; ?>%)" pos="<?php echo $pos; ?>">
                <a href="CategoryDetails">    
                <div class="decoration_img" style="background-image: url(<?php echo base_url('assets/klixaja/images/category4.jpeg')?>)"></div>
                <p class="decoration_title">Kategori <?php echo $i; ?></p>
                            </a>
            </div>
            <?php } ?>
        </div>
        </a>
    </div>
    <!-- produk siap santap -->
    <!-- produk siap santap -->
        
 <div class="padding_box_2wh"  >
            <div class="bg" data-aos="fade-up">
                <div class="detail">
                    <h1 style="color: #000;">Makanan & Minuman X-press</h1>
                    <p><a href="">View All</a> </p>
                </div>
            </div>
        
        <div style="margin-left: 0;">
        <div class="slider lebarrr">
            <?php
            for($i=1;$i<21;$i++){
                $rand = rand(1,10);
                ?>
                <div class="wid" style="outline:none;margin-right:-1.5px "data-aos="fade-up" data-aos-delay=<?php echo $i."00"; ?>>
                    <div class="bentukkkkk"  >
                        <div style="margin-top:2px;margin-left:2px;font-size:12px" class="decoration_label_box posisi_absolute"><span>Star Seller</span></div>
                  <div class="decoration_label_box decoration_label_box_2 posisi_absolute" style="right:0;margin-top:2px;margin-right:6px"><span>33%</span><span>Off</span></div>
                        <a  href="Details" style="outline:none;">
                            <img src="<?php echo base_url('assets/klixaja/images/siap_santap1.jpeg')?>" alt="">
                            <div class="max_line">
                                <p class="font_judul" >Martabak Spesial vegetarian asal palembang</p>
                            </div>
                        </a>
                            <div class="image_desc">
                                <div class="image_desc_detail">
                                    <i class="heart_icon icon" title="Tambahkan ke wishlist"></i>
                                    <div class="rating sized_box col_auto_max_content_5">
                                        <label class="parent star full">&starf;</label>
                                        <label class="parent star full">&starf;</label>
                                        <label class="parent star full">&starf;</label>
                                        <label class="parent star half">&starf;</label>
                                        <label class="parent star empty">&starf;</label>
                                    </div>
                                </div>
                                <a href="Details" style="outline:none;">
                                <div class="harga">
                                    <p class=" decoration_sale">Rp1.250.000</p>
                                    <p class="decoration_discount">Rp1.000.000</p>
                                    <!-- grid,gap=10px,  grid-template-columns: max-content max-content; -->

                                </div>
                                </a>
                            </div>
                    </div>
                </div>
            <?php } ?>
</div>
        </div>

        <!-- untuk banner -->
        <div style="margin-top:25px;" class="padding_box_2wh">
            <div class="container_banner">
                <?php
                for($i=1; $i<4; $i++){
                    ?>
                    <div class="banner_bentuk" data-aos="fade-up" data-aos-delay=<?php echo $i."00" ?>>
                        <img src="<?php echo base_url('assets/klixaja/images/siap_santap3.jpeg'); ?>" alt="">
                    </div>
                <?php }?>
            </div>
        </div>


        <div class="padding_box_2wh">
            <div class="bg" data-aos="fade-up">
                <div class="detail">
                    <h1 style="color: #000;">Produk Pre Order</h1>
                    <p><a href="">View All</a> </p>
                </div>
            </div>
                    <div style="margin-left: 0px;">
            <div class="slider lebarrr">
                <?php
                for($i=1;$i<21;$i++){
                    $rand = rand(1,5);
                    ?>
                    <div class="wid" style="outline:none;margin-top:10px;margin-bottom:10px;margin-left:10px;margin-right:-1.5px" data-aos="fade-up" data-aos-delay=<?php echo $i."00"; ?>>
                        <div class="bentukkkkk"
                        >
                            <div style="margin-top:2px;margin-left:2px;font-size:12px" class="decoration_label_box posisi_absolute"><span>Star Seller</span></div>
                            <div class="decoration_label_box decoration_label_box_2 posisi_absolute" style="right:0;margin-top:2px;margin-right:6px"><span>33%</span><span>Off</span></div>
                            <a  href="Details" style="outline:none;">
                            <img src="<?php echo base_url('assets/klixaja/images/siap_santap2.jpeg'); ?>" alt="">

                                <!-- <div class="tombol" >
                                    <div class="desain_button" >
                                    <a href=""><button class="bg_button_addtocart"type="button">Add to cart</button></a>  <br>
                                   <a href=""><button class="bg_button_quickview" type="button">Quick View</button></a>
                                    </div>
                                </div> -->

                                <div class="max_line">
                                    <p class="font_judul" >Martabak Spesial vegetarian asal palembang</p>
                                </div>
                            </a>

                                <div class="image_desc">
                                    <div class="image_desc_detail">
                                        <i class="heart_icon icon " title="Tambahkan ke wishlist"></i>
                                        <div class="rating sized_box col_auto_max_content_5">
                                            <label class="parent star full">&starf;</label>
                                            <label class="parent star full">&starf;</label>
                                            <label class="parent star full">&starf;</label>
                                            <label class="parent star half">&starf;</label>
                                            <label class="parent star empty">&starf;</label>
                                        </div>
                                    </div>
                                    <a href="Details" style="outline:none;">
                                    <div class="harga">
                                        <!-- <p class=" decoration_sale">Rp1.250.000</p> -->
                                        <p class="decoration_discount">Rp1.000.000</p>
                                        <!-- grid,gap=10px,  grid-template-columns: max-content max-content; -->

                                    </div>
                                    </a>
                                </div>

                        </div>
                    </div>
                <?php } ?>
            </div>
                    </div>





            <div style="margin-top: 20px;" class="bg" data-aos="fade-up">
                <div class="detail">
                    <h1 style="color: #000;">Produk Terbaru</h1>
                    <p><a href="">View All</a> </p>
                </div>
            </div>

            <div class="contain">
                <?php
                for($i=1;$i<21;$i++){
                    $rand = rand(1,5);
                    ?>
                    <div data-aos="fade-up" data-aos-delay=<?php echo $i."00"; ?>>

                        <div class="bentuk"
                        >
                            <div style="font-size:12px" class="decoration_label_box posisi_absolute"><span>Star Seller</span></div>
                            <div class="decoration_label_box decoration_label_box_2 posisi_absolute" style="right:0;margin-right:4px"><span>33%</span><span>Off</span></div>
                            <a  href="Details">
                            <img src="<?php echo base_url('assets/klixaja/images/siap_santap1.jpeg'); ?>" alt="">

                                <!-- <div class="tombol" >
                                    <div class="desain_button" >
                                    <a href=""><button class="bg_button_addtocart"type="button">Add to cart</button></a>  <br>
                                   <a href=""><button class="bg_button_quickview" type="button">Quick View</button></a>
                                    </div>
                                </div> -->

                                <div class="max_line">
                                    <p class="font_judul" >Martabak Spesial vegetarian asal palembang</p>
                                </div>
                            </a>
                                <div class="image_desc">
                                    <div class="image_desc_detail">
                                        <i class="heart_icon icon " title="Tambahkan ke wishlist"></i>
                                        <div class="rating sized_box col_auto_max_content_5">
                                            <label class="parent star full">&starf;</label>
                                            <label class="parent star full">&starf;</label>
                                            <label class="parent star full">&starf;</label>
                                            <label class="parent star half">&starf;</label>
                                            <label class="parent star empty">&starf;</label>
                                        </div>
                                    </div>
                                    <a href="Details">
                                    <div class="harga">
                                        <!-- <p class=" decoration_sale">Rp1.250.000</p> -->
                                        <p class="decoration_discount">Rp1.000.000</p>
                                        <!-- grid,gap=10px,  grid-template-columns: max-content max-content; -->

                                    </div>
                                    </a>
                                </div>

                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>


        <div class="padding_box_2wh">
            <div class="bg" data-aos="fade-up">
                <div class="detail">
                    <h1 style="color: #000;">Produk Terlaris</h1>
                    <p><a href="">View All</a> </p>
                </div>
            </div>
  
                <div style="margin-left: 10px;">

        <div class="slider lebarrr">
            <?php
            for($i=1;$i<21;$i++){
                $rand = rand(1,5);
                ?>
                <div class="wid"style="outline:none;margin-top:10px;margin-bottom:10px;margin-left:10px;margin-right:-1.5px" data-aos="fade-up" data-aos-delay=<?php echo $i."00"; ?>>

                    <div class="bentukkkkk"
                    >
                       <div style="margin-top:2px;margin-left:2px;font-size:12px" class="decoration_label_box posisi_absolute"><span>Star Seller</span></div>
                            <div class="decoration_label_box decoration_label_box_2 posisi_absolute" style="right:0;margin-top:2px;margin-right:5px"><span>33%</span><span>Off</span></div>
                        <a  href="Details" style="outline:none;">
                            <img src="<?php echo base_url('assets/klixaja/images/siap_santap1.jpeg'); ?>" alt="">

                            <!-- <div class="tombol" >
                                <div class="desain_button" >
                                <a href=""><button class="bg_button_addtocart"type="button">Add to cart</button></a>  <br>
                               <a href=""><button class="bg_button_quickview" type="button">Quick View</button></a>
                                </div>
                            </div> -->

                            <div class="max_line">
                                <p class="font_judul" >Martabak Spesial vegetarian asal palembang</p>
                            </div>
                        </a>

                            <div class="image_desc">
                                <div class="image_desc_detail">
                                    <i class="heart_icon icon " title="Tambahkan ke wishlist"></i>
                                    <div class="rating sized_box col_auto_max_content_5">
                                        <label class="parent star full">&starf;</label>
                                        <label class="parent star full">&starf;</label>
                                        <label class="parent star full">&starf;</label>
                                        <label class="parent star half">&starf;</label>
                                        <label class="parent star empty">&starf;</label>
                                    </div>
                                </div>
                                <a href="Details" style="outline:none;">
                                <div class="harga">
                                    <!-- <p class=" decoration_sale">Rp1.250.000</p> -->
                                    <p class="decoration_discount">Rp1.000.000</p>
                                    <!-- grid,gap=10px,  grid-template-columns: max-content max-content; -->

                                </div>
                                </a>
                            </div>

                    </div>
                </div>
            <?php } ?>
        </div>
                </div>
    </div>


    <div class="garis"></div>


    <div class="padding_box_2wh">
        <div class="info">
        <div class="belanjaaa">
            <h2>Kontak Kami</h2>
            <p><a href="" style="pointer-events: none;"><i class="fas fa-phone-square-alt"> 0851-2354-2121(08.00 - 18.00)</i></a></p>
            <p><a href="" style="text-decoration:underline;color:blue"><i style="color: #666"class="fas fa-envelope"></i> klix@gmail.com</a></p>
            
        </div>
<div class="belanja">
                <h2>Pembeli</h2>
                <p><a href="PanduanBelanja">Panduan Belanja</a></p>
                <p><a href="">Pembatalan dan Pengembalian</a></p>
            </div>
            <div class="belanja">
                <h2>Penjual</h2>
                <p><a href="">Alasan Jadi Penjual</a></p>
                <p><a href="">Cara Menjadi Penjual</a></p>
                <p><a href="">Ketentuan Produk</a></p>
                <p><a href="">Ketentuan Penjual</a></p>

            </div>

            <div class="belanja">
                <h2>Klixaja</h2>
                <p><a href="TentangKami">Tentang Kami</a></p>
                <p><a href="VisiMisi">Visi & Misi</a></p>
                <p><a href="">Syarat & Ketentuan</a></p>
                <p><a href="">FAQs</a></p>
                <p><a href="">Blog</a></p>
            </div>
            <div class="belanja">
                <h2>Download Aplikasi</h2>
                <a href=""><img src="<?php echo base_url('assets/klixaja/images/icons/google_play.png'); ?>" alt=""></a>
                <a href=""><img src="<?php echo base_url('assets/klixaja/images/icons/ios.png'); ?>" alt=""></a>
            </div>
        </div>
    </div>

    <!-- garis===================================================================== -->

    <div class="garis" style="margin-top: 70px;"></div>

    <div class="padding_box_2wh">
        <div class="kolom2">
            <div class="pengiriman">
                <h2>Pengiriman</h2>
                <img src="<?php echo base_url('assets/klixaja/images/icons/icon_pengiriman.jpeg'); ?>" alt="">
            </div>

            <div class="pembayaran">
                <h2>Pembayaran</h2>
                <img src="<?php echo base_url('assets/klixaja/images/icons/icon_pembayaran.jpeg'); ?>" alt="">
            </div>
        </div>



    </div>

    <div class="garis" ></div>

    <div class="padding_box_2wh">
        <div class="dis">
            <p style="margin-left:30px;">&copy 2020 klixaja. All Rights Reserved</p>
        </div>
    </div>


</section>


</div>


