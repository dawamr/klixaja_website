<script src="<?php echo base_url('assets/klixaja/js/lib/jquery-3.5.1.min.js'); ?>"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

    <script>
          $(".heart_icon").on("click", function () {
    if ($(this).hasClass("fill")) {
      $(this).removeClass("fill");
    } else {
      $(this).addClass("fill");
    }
  });




    </script>
    <script>
/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction() {
  document.getElementById("myDropdown").classList.toggle("showww");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.user_icon')) {
    var dropdowns = document.getElementsByClassName("dropdown-contenttt");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('showww')) {
        openDropdown.classList.remove('showww');
      }
    }
  }
}
</script>
<script src="<?php echo base_url('assets/aos/dist/aos.js')?>"></script>
<script>
    AOS.init();
</script>
<script src="<?php echo base_url('assets/klixaja/js/script.js')?>"></script>
<script src="<?php echo base_url('assets/klixaja/js/nav.js')?>"></script>
<script src="<?php echo base_url('assets/klixaja/js/home.js')?>"></script>

<script src='<?php echo base_url('assets/klixaja/js/slick.min.js')?>'></script>
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script> -->

<script>
  $('.slider').slick({
  infinite: false,
  speed: 1000,
  slidesToShow: 5,
  slidesToScroll: 5,

  responsive: [
       {
      breakpoint: 940,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: false
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        infinite: false
      }
    },
    {
      breakpoint: 550,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 2,
        slidesToScroll:2
      }
    }
    
  ]
});
</script>



  <script>

  $('.sliderrr').slick({
  infinite: true,
  speed: 1000,
  dots: true,
  slidesToShow: 1,
  slidesToScroll:  1,
      autoplay:true,
  autoplaySpeed:2500,

   responsive: [
       {
      breakpoint: 940,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: false
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        infinite: false
      }
    },
    {
      breakpoint: 550,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 2,
        slidesToScroll:2
      }
    }
    
  ]
  });

</script>

<script>
  $(document).ready(function () {
  var animation_interval = 5000;
  var time = 0;
  $right_banner_images = null;

  carousel_init();

  $(".cart").on("mouseover", function () {
    if ($(window).width() > 650) {
      $(".cart_isosceles, .cart_content").fadeIn();
    }
  });

  $(".cart").on("mouseleave", function () {
    $(".cart_isosceles, .cart_content").fadeOut();
  });

  $(".left_banner .left").on("click", function (e) {
    carousel_animate("left");
    time = new Date().getTime();
  });

  $(".left_banner .right").on("click", function () {
    carousel_animate("right");
    time = new Date().getTime();
  });

  $("#category .left, #category .right").on("mouseover", function () {
    var img_boxes = $("#category .decoration_box_1");
    if ($(this).hasClass("left")) {
      var first_img_left = Math.round(
        convert_to_number(img_boxes.eq(0).css("left"))
      );
      if (first_img_left < 0) {
        $(this).animate(
          {
            opacity: 1.0,
          },
          {
            duration: "fast",
          }
        );
      }
    }

    if ($(this).hasClass("right")) {
      var img_width = Math.round(convert_to_number(img_boxes.css("width")));
      var last_img_left = Math.round(
        convert_to_number(img_boxes.eq(img_boxes.length - 1).css("left"))
      );
      if (last_img_left > img_width * 10) {
        $(this).animate(
          {
            opacity: 1.0,
          },
          {
            duration: "fast",
          }
        );
      }
    }
  });

  $("#category .left, #category .right").on("mouseleave", function () {
    $(this).animate(
      {
        opacity: 0.0,
      },
      {
        duration: "fast",
      }
    );
  });

  $("#category .left, #category .right").on("click", function () {
    var img_boxes = $("#category .decoration_box_1");
    var img_width = Math.round(convert_to_number(img_boxes.css("width")));
    if ($(this).hasClass("left")) {
      var first_img_left = Math.round(
        convert_to_number(img_boxes.eq(0).css("left"))
      );
      if (first_img_left < 0) {
        $.each(img_boxes, function (i) {
          var left = Math.round(
            (convert_to_number($(this).css("left"), true) / img_width) * 10
          );
          $(this).animate(
            {
              left: left + 10 + "%",
            },
            {
              duration: 500,
            }
          );
        });
      }
    }

    if ($(this).hasClass("right")) {
      var last_img_left = Math.round(
        convert_to_number(img_boxes.eq(img_boxes.length - 1).css("left"))
      );
      if (last_img_left > img_width * 10) {
        $.each(img_boxes, function (i) {
          var left = Math.round(
            (convert_to_number($(this).css("left"), true) / img_width) * 10
          );
          $(this).animate(
            {
              left: left - 10 + "%",
            },
            {
              duration: 500,
            }
          );
        });
      }
    }
  });

  $("#popular_product .left, #popular_product .right").on(
    "mouseover",
    function () {
      var img_boxes = $("#popular_product .decoration_img_box");
      if ($(this).hasClass("left")) {
        var first_img_left = Math.round(
          convert_to_number(img_boxes.eq(0).css("left"))
        );
        if (first_img_left < 0) {
          $(this).animate(
            {
              opacity: 1.0,
            },
            {
              duration: "fast",
            }
          );
        }
      }

      if ($(this).hasClass("right")) {
        var img_width = Math.round(convert_to_number(img_boxes.css("width")));
        var last_img_left = Math.round(
          convert_to_number(img_boxes.eq(img_boxes.length - 1).css("left"))
        );
        if (last_img_left > img_width * 4) {
          $(this).animate(
            {
              opacity: 1.0,
            },
            {
              duration: "fast",
            }
          );
        }
      }
    }
  );

  $("#popular_product .left, #popular_product .right").on("click", function () {
    var img_boxes = $("#popular_product .decoration_img_box");
    var img_width = Math.round(convert_to_number(img_boxes.css("width")));
    if ($(this).hasClass("left")) {
      var first_img_left = Math.round(
        convert_to_number(img_boxes.eq(0).css("left"))
      );
      if (first_img_left < 0) {
        $.each(img_boxes, function (i) {
          var left = Math.round(
            (convert_to_number($(this).css("left"), true) / (img_width * 4)) *
              100
          );
          $(this).animate(
            {
              left: left + 25 + "%",
            },
            {
              duration: 500,
            }
          );
        });
      }
    }

    if ($(this).hasClass("right")) {
      var last_img_left = Math.round(
        convert_to_number(img_boxes.eq(img_boxes.length - 1).css("left"))
      );
      if (last_img_left > img_width * 4) {
        $.each(img_boxes, function (i) {
          var left = Math.round(
            (convert_to_number($(this).css("left"), true) / (img_width * 4)) *
              100
          );
          $(this).animate(
            {
              left: left - 25 + "%",
            },
            {
              duration: 500,
            }
          );
        });
      }
    }
  });

  $(window).on("resize", function () {
    carousel_resize_listener();
    resize_nav_arrow();
    var img_boxes = $("#category .img_box");
    for (var i = 0, j = 0; i < img_boxes.length; i += 2, j += 10) {
      var img_box1 = img_boxes.eq(i);
      var img_box2 = img_boxes.eq(i + 1);
      img_box1.css("left", j + "%");
      img_box2.css("left", j + "%");
    }
  });

  function resize_nav_arrow() {
    var parent_height = convert_to_number(
      $(".carousel").parent().css("height")
    );
    var nav_arrow = $(".carousel_arrow");
    var nav_arrow_height = convert_to_number(nav_arrow.css("height"));
    nav_arrow.css("top", parent_height / 2 - nav_arrow_height / 2);

    var parent_height =
      convert_to_number($("#category").children().eq(0).css("height")) + 10;
    var category_box = convert_to_number(
      $("#category .slider_box").css("height")
    );
    var nav_arrow = $("#category .nav_arrow");
    var nav_arrow_height = convert_to_number(nav_arrow.css("height"));
    nav_arrow.css(
      "top",
      parent_height + category_box / 2 - nav_arrow_height / 2 + "px"
    );
  }

  function carousel_animate(slide_direction, interval_time) {
    var img_list = $(".carousel").children();
    var img_width = convert_to_number(img_list.eq(0).css("width"), true);
    var active_img = img_list.filter(".active");
    var active_index = active_img.index();
    var active_left = convert_to_number(active_img.css("left"), true);
    var duration = 500;

    if (interval_time - time < animation_interval) return;

    if (slide_direction == "left") {
      if (active_left != 0) return;
      active_img.removeClass("active");
      if (active_index != 0) {
        img_list.eq(active_index - 1).addClass("active");
        $.each(img_list, function (i) {
          var img = $(this);
          var left = convert_to_number(img.css("left"), true);
          img.animate(
            {
              left: left + img_width + "px",
            },
            {
              duration: duration,
              complete: function () {
                var pos = parseInt(img.attr("pos"));
                img.attr("pos", pos + 1);
              },
            }
          );
        });
      } else {
        img_list.eq(img_list.length - 1).addClass("active");
        $.each(img_list, function (i) {
          var img = $(this);
          img.attr("pos", i - (img_list.length - 1));
          if (i != img_list.length - 1) {
            img.css("left", i * img_width + "px");
          } else {
            img.css("left", -img_width + "px");
          }
        });

        $.each(img_list, function (i) {
          $(this).animate(
            {
              left:
                i != img_list.length - 1 ? i * img_width + img_width : 0 + "px",
            },
            {
              duration: duration,
              complete: function () {
                if (i != img_list.length - 1) {
                  $(this).css(
                    "left",
                    -((img_list.length - 1 - i) * img_width) + "px"
                  );
                }
              },
            }
          );
        });
      }
    }

    if (slide_direction == "right") {
      if (active_left != 0) return;
      active_img.removeClass("active");
      if (active_index != img_list.length - 1) {
        img_list.eq(active_index + 1).addClass("active");
        $.each(img_list, function (i) {
          var img = $(this);
          var left = convert_to_number(img.css("left"), true);
          img.animate(
            {
              left: left - img_width + "px",
            },
            {
              duration: duration,
              complete: function () {
                var pos = parseInt(img.attr("pos"));
                img.attr("pos", pos - 1);
              },
            }
          );
        });
      } else {
        img_list.eq(0).addClass("active");
        $.each(img_list, function (i) {
          var img = $(this);
          img.attr("pos", i);
          if (i != img_list.length - 1) {
            img.css("left", i * img_width + img_width + "px");
          }
        });

        $.each(img_list, function (i) {
          $(this).animate(
            {
              left:
                i != img_list.length - 1 ? i * img_width : -img_width + "px",
            },
            {
              duration: duration,
              complete: function () {
                if (i == img_list.length - 1) {
                  $(this).css("left", i * img_width + "px");
                }
              },
            }
          );
        });
      }
    }
  }

  function carousel_init() {
    var img_list = $(".carousel").children();
    var img_width = convert_to_number(img_list.eq(0).css("width"), true);
    $.each(img_list, function (i) {
      $(this).css("left", i * img_width + "px");
    });

    carousel_resize_listener();

    if (animation_interval > 0) {
      setInterval(() => {
        carousel_animate("right", new Date().getTime());
      }, animation_interval);
    }
  }

  function carousel_resize_listener() {
    var width = $(window).width();

    var img_list = $(".carousel").children();
    var img_width = convert_to_number(img_list.eq(0).css("width"), true);
    $.each(img_list, function (i) {
      var img = $(this);
      var pos = parseInt(img.attr("pos"));
      img.css("left", pos * img_width + "px");
    });

    if (width <= 650) {
      if ($right_banner_images == null) {
        $right_banner_images = $(".right_banner_img")
          .detach()
          .addClass("carousel_img");
        var lowest_pos = 0;
        var highest_pos = 0;
        $.each(img_list, function (i) {
          var img = $(this);
          var pos = parseInt(img.attr("pos"));
          if (pos > highest_pos) {
            highest_pos = pos;
          }

          if (pos < lowest_pos) {
            lowest_pos = pos;
          }
        });

        var next_pos;
        if (Math.abs(lowest_pos) > highest_pos) {
          next_pos = lowest_pos - 1;
        } else {
          next_pos = highest_pos + 1;
        }

        $.each($right_banner_images, function (i) {
          var img = $(this);
          if (next_pos < 0) {
            img.attr("pos", next_pos);
            img.css("left", next_pos-- * img_width);
          } else if (next_pos > 0) {
            img.attr("pos", next_pos);
            img.css("left", next_pos++ * img_width);
          }
        });
        $(".carousel").append($right_banner_images);
      }
    } else {
      if ($right_banner_images != null) {
        $(".carousel").children().filter(".right_banner_img").remove();
        img_list = $(".carousel").children();
        img_list.eq(0).addClass("active");
        $(".right_banner").append(
          $right_banner_images
            .removeClass("carousel_img active")
            .removeAttr("pos")
        );
        $.each(img_list, function (i) {
          var img = $(this);
          img.attr("pos", i).css("left", i * img_width + "px");
        });

        $right_banner_images = null;
      }
    }
  }
});

</script>

<script>$(document).ready(function () {
  if ($("#category").length > 0) {
    $("#category .left, #category .right").on("mouseover", function () {
      var img_boxes = $("#category .decoration_box_1");
      if ($(this).hasClass("left")) {
        var first_img_left = Math.round(
          convert_to_number(img_boxes.eq(0).css("left"))
        );
        if (first_img_left < 0) {
          $(this).animate(
            {
              opacity: 1.0,
            },
            {
              duration: "fast",
            }
          );
        }
      }

      if ($(this).hasClass("right")) {
        var img_width = Math.round(convert_to_number(img_boxes.css("width")));
        var last_img_left = Math.round(
          convert_to_number(img_boxes.eq(img_boxes.length - 1).css("left"))
        );
        if (last_img_left > img_width * 10) {
          $(this).animate(
            {
              opacity: 1.0,
            },
            {
              duration: "fast",
            }
          );
        }
      }
    });

    $("#category .left, #category .right").on("mouseleave", function () {
      $(this).animate(
        {
          opacity: 0.0,
        },
        {
          duration: "fast",
        }
      );
    });

    $("#category .left, #category .right").on("click", function () {
      var img_boxes = $("#category .decoration_box_1");
      var img_width = Math.round(convert_to_number(img_boxes.css("width")));
      if ($(this).hasClass("left")) {
        var first_img_left = Math.round(
          convert_to_number(img_boxes.eq(0).css("left"))
        );
        if (first_img_left < 0) {
          $.each(img_boxes, function (i) {
            var left = Math.round(
              (convert_to_number($(this).css("left"), true) / img_width) * 10
            );
            $(this).animate(
              {
                left: left + 10 + "%",
              },
              {
                duration: 500,
              }
            );
          });
        }
      }

      if ($(this).hasClass("right")) {
        var last_img_left = Math.round(
          convert_to_number(img_boxes.eq(img_boxes.length - 1).css("left"))
        );
        if (last_img_left > img_width * 10) {
          $.each(img_boxes, function (i) {
            var left = Math.round(
              (convert_to_number($(this).css("left"), true) / img_width) * 10
            );
            $(this).animate(
              {
                left: left - 10 + "%",
              },
              {
                duration: 500,
              }
            );
          });
        }
      }
    });

    $(window).on("resize", function () {
      var img_boxes = $("#category .img_box");
      for (var i = 0, j = 0; i < img_boxes.length; i += 2, j += 10) {
        var img_box1 = img_boxes.eq(i);
        var img_box2 = img_boxes.eq(i + 1);
        img_box1.css("left", j + "%");
        img_box2.css("left", j + "%");
      }
      resize();
    });

    resize();

    function resize() {
      var parent_height =
        convert_to_number($("#category").children().eq(0).css("height")) + 10;
      var category_box = convert_to_number(
        $("#category .slider_box").css("height")
      );
      var nav_arrow = $("#category .nav_arrow");
      var nav_arrow_height = convert_to_number(nav_arrow.css("height"));
      nav_arrow.css(
        "top",
        parent_height + category_box / 2 - nav_arrow_height / 2 + "px"
      );
    }
  }

  $(".heart_icon").on("click", function () {
    if ($(this).hasClass("fill")) {
      $(this).removeClass("fill");
    } else {
      $(this).addClass("fill");
    }
  });
});

function convert_to_number(value, round = false) {
  value = parseFloat(value.replace(/[^\d\-\.]/g, ""));
  if (round) {
    return Math.round(value);
  }

  return value;
}
</script>
