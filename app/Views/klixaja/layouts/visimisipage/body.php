<div class=" container containe mtop"  >
    <div class=""  >
        <h3 class="text-visi">Visi & Misi</h3>
     <img class="klixlogo mx-auto mt-2 " src="<?php echo base_url('assets/klixaja/images/klixajalogo.png');?>" alt="klixaja logo">
    </div>

    <h1 class=" font-visimisi
">Visi</h1>
    <div class="row mt-4 " data-aos="fade-right" >
        <div style="padding-right:0"class="col-lg-3 col-md-4 col-sm-3 col-3" data-aos="zoom-in-down">
            <img class="img-visi" src="<?php echo base_url('assets/klixaja/images/visi1.png')?>" alt="">
        </div>
        <div style="" class="my-auto col-lg-9 col-md-8 col-sm-9 col-9">
            <p class="visi-font">
              Menjadi marketplace produk sehat nabati terlengkap dan terpercaya yang selaras dengan kelestarian alam.
            </p>
        </div>
    </div>

    <h1 style="margin-top:20px;" class="font-visimisi">Misi</h1>
      <div class="row mt-2"  data-aos="fade-left">
        <div class="my-auto col-lg-9 col-md-8 col-sm-9 col-9">
            <p class="visi-font1">
Memberdayakan rumah makan sehat nabati, pelaku usaha dan UMKM produk sehat nabati.
            </p>
        </div>
         <div class="col-lg-3 col-md-4 col-sm-3 col-3"  data-aos="zoom-in-down">
            <img class="img-visi1" src="<?php echo base_url('assets/klixaja/images/visi2.jpg')?>" alt="">
        </div>
    </div>

<div class="row mt-4" data-aos="fade-right">
        <div class="col-lg-3 col-md-4 col-sm-3 col-3" data-aos="zoom-in-down">
            <img class="img-visi" src="<?php echo base_url('assets/klixaja/images/visi3.jpg')?>" alt="">
        </div>
        <div class="my-auto col-lg-9 col-md-8 col-sm-9 col-9">
            <p class="visi-font">
Memudahkan masyarakat mendapatkan produk sehat nabati.            </p>
        </div>
</div>


    <div class="row mt-4"  data-aos="fade-left">
        <div class="my-auto col-lg-9 col-md-8 col-sm-9 col-9">
            <p class="visi-font1">
Mengedukasi masyarakat tentang pentingnya pola hidup sehat nabati yang selaras dengan alam melalui blog edukasi.             </p>
        </div>
         <div class="col-lg-3 col-md-4 col-sm-3 col-3"  data-aos="zoom-in-down">
            <img class="img-visi1" src="<?php echo base_url('assets/klixaja/images/visi4.jpg')?>" alt="">
        </div>
    </div>


<div class="row mt-4" data-aos="fade-right">
        <div class="col-lg-3 col-md-4 col-sm-3 col-3" data-aos="zoom-in-down">
            <img class="img-visi" src="<?php echo base_url('assets/klixaja/images/visi5.jpg')?>" alt="">
        </div>
        <div class="my-auto col-lg-9 col-md-8 col-sm-9 col-9">
            <p class="visi-font">
Bekerjasama dengan organisasi yang memiliki visi dan misi yang sama untuk sosialisasi pola hidup sehat nabati.            </p>
        </div>
</div>

 <div class="row mt-4"  data-aos="fade-left">
        <div class="my-auto     col-lg-9 col-md-8 col-sm-9 col-9">
            <p class="visi-font1">
Menjunjung, mencintai dan memuliakan semua bentuk kehidupan melalui pola hidup sehat nabati yang selaras dengan alam.        </div>
         <div class="col-lg-3 col-md-4 col-sm-3 col-3"  data-aos="zoom-in-down">
            <img class="img-visi1" src="<?php echo base_url('assets/klixaja/images/visi6.jpg')?>" alt="">
        </div>
    </div>

       <div class="garisss" style="margin-top: 20px;"></div>
                     <div class="mt-5">
            <div class="info">
                <div class="belanjaa">
                   <h2>Kontak Kami</h2>
            <p><a href="" style="pointer-events: none;"><i class="fas fa-phone-square-alt"> 0851-2354-2121(08.00 - 18.00)</i></a></p>
            <p><a href="" style="text-decoration:underline;color:blue"><i style="color: #666"class="fas fa-envelope"></i> klix@gmail.com</a></p>
            
        </div>
<div class="belanjaa">
                <h2>Pembeli</h2>
                <p><a href="">Panduan Belanja</a></p>
                <p><a href="">Pembatalan dan Pengembalian</a></p>
            </div>
            <div class="belanjaa">
                <h2>Penjual</h2>
                <p><a href="">Alasan Jadi Penjual</a></p>
                <p><a href="">Cara Menjadi Penjual</a></p>
                <p><a href="">Ketentuan Produk</a></p>
                <p><a href="">Ketentuan Penjual</a></p>

            </div>

            <div class="belanjaa">
                <h2>Klixaja</h2>
                <p><a href="">Tentang Kami</a></p>
                <p><a href="">Visi & Misi</a></p>
                <p><a href="">Syarat & Ketentuan</a></p>
                <p><a href="">FAQs</a></p>
                <p><a href="">Blog</a></p>
            </div>
            <div class="belanjaa">
                <h2>Download Aplikasi</h2>
                <a href=""><img src="<?php echo base_url('assets/klixaja/images/icons/google_play.png'); ?>" alt=""></a>
                <a href=""><img src="<?php echo base_url('assets/klixaja/images/icons/ios.png'); ?>" alt=""></a>
            </div>
            </div>
        </div>
        
        <!-- garis===================================================================== -->
        
      <div class="garissss" style="margin-top:70px"  ></div>

                <div class=" mt-5 d-sm-none d-md-block">
                        <div class="kolom22 mt-5">
                            <div class="pengiriman">
                            <h2>Pengiriman</h2>
                            <img src="<?php echo base_url('assets/klixaja/images/icons/icon_pengiriman.jpeg')?>" alt="">
                            </div>

                             <div class="pembayarann">
                            <h2>Pembayaran</h2>
                            <img src="<?php echo base_url('assets/klixaja/images/icons/icon_pembayaran.jpeg')?>" alt="">
                        </div>
                        </div>

                </div>

            <div class="garisss mt-2" ></div>

                <div class="padding_box_2 wh mt-3">
                    <div class="dis">
                        <p style="margin-left:30px;">&copy 2020 klixaja. All Rights Reserved</p>
                        <!-- <div class="iconv">
                            <p>We Using Safe Payment For</p>
                
                            <img src="assets/klixaja/images/icons/icon_bca.png";?>" alt="">
                            <img style="margin-left:18px;" src="./assets/klixaja/images/icons/icon_mandiri.gif";?>" alt="">
                        </div> -->
                    </div>
                </div>
