<div class="container inp mtop min-width" >
    <h4 class="text-visi1">Ketentuan Umum Produk</h4>
    <p class="font-visimisi font-size " style="font-size:22px">1. Produk yang dilarang</p>
    <div class="container">
        
    <div style="display:flex">
        <p class="visi-font2 mr-1">a.</p>
    
    <p class="visi-font2">Produk hewani yang melalui proses penyembelihan beserta turunan
    nya seperti gelatin, asam amino, mineral, gliserol, lemak, emulsifier,
    serbuk kalsium dan lain sebagainya.</p>
    </div>


     <div style="display:flex">
        <p class="visi-font2 mr-1">b.</p>
    
    <p class="visi-font2"> Enzim, darah, kelenjar, dan organ (jeroan).</p>
    </div>

    <div style="display:flex">
        <p class="visi-font2 mr-1">c.</p>
    
    <p class="visi-font2"> Produk laut dan perairan seperti jenis ikan, udang, cumi, dll beserta olahannya seperti sosis, nugget dan olahan lainnya.</p>
    </div>

    <div style="display:flex">
        <p class="visi-font2 mr-1">d.</p>
    
    <p class="visi-font2"> Bumbu-bumbu masakan, pewarna makanan, pengental, pengawet baik yang berasal dari hewani maupun turunannya.</p>
    </div>
    <div style="display:flex">
        <p class="visi-font2 mr-1">e.</p>
    
    <p class="visi-font2"> Jenis bawang seperti bawang bombai, bawang merah, bawang putih, daun bawang, kucai/garlic chives atau jenis bawang lainnya.</p>
    </div>
    <div style="display:flex">
        <p class="visi-font2 mr-1">f.</p>
    
    <p class="visi-font2"> Produk yang mengandung bahan kimia berbahaya atau obat-obatan terlarang.</p>
    </div>
    <div style="display:flex">
        <p class="visi-font2 mr-1">g.</p>
    
    <p class="visi-font2"> Minuman beralkohol.</p>

    </div>
    <div class="container"  style="margin-left:20px;margin-right:20px">
        <div style="display:flex">
        <p class="visi-font2 mr-1">a.</p>
    
    <p class="visi-font2"> Produk suplemen dan vitamin yang tidak mendapat ijin dari BPOM atau Dinas Kesehatan .</p>
</div>
    <div style="display:flex">
        <p class="visi-font2 mr-1">b.</p>
    
    <p class="visi-font2"> Produk suplemen dan vitamin yang terdapat kandungan unsur hewani.</p>
</div>
   
        <div style="display:flex">
            <p class="visi-font2 mr-1">c.</p>
        
        <p class="visi-font2"> Makanan dan minuman lain yang melanggar ketentuan hukum yang berlaku di Indonesia.</p>
        </div>
    </div>

      <div style="display:flex">
        <p class="visi-font2 mr-1">h.</p>
    
    <p class="visi-font2"> Komposisi produk yang mengandung hewani sesuai ketentuan yang telah ditetapkan oleh Indonesia Vegetarian Society (IVS) terkait bahan- bahan yang berasal dari hewan. (bisa akses di
         <a class="visi-font2" style="color:blue" href="https://elated.co.za/animal-derived-ingredients/">https://elated.co.za/animal-derived-ingredients/</a>  ).</p>
    </div>
    </div>

        <p class="font-visimisi font-size  " style="font-size:22px">2. Jenis Produk</p>
        <div class="container">
        <div style="display:flex">
        <p class="visi-font2 mr-1">a.</p>
    
    <p class="visi-font2"> Produk Vegan <br> Produk suplemen dan vitamin yang tidak mendapat ijin dari BPOM atau Dinas Kesehatan.</p>
</div>

 <div style="display:flex">
        <p class="visi-font2 mr-1">b.</p>
    
    <p class="visi-font2"> Produk Vegetarian <br> Produk yang tidak mengandung unsur daging, namun masih mengandung unsur olahan dari telur, keju, madu atau susu.</p>
</div>
        </div>

                <p class="font-visimisi font-size font-size " style="font-size:22px">3. Produk Minuman Dan Makanan X-Press</p>
    
    <div class="container">

        <p class="visi-font2"> Produk makanan dan minuman X-press diartikan sebagai produk yang diproduksi oleh produsen kemudian disalurkan langsung kepada konsumen untuk dikonsumsi (biasanya homemade).  Produk makanan dan minuman X-press lebih cenderung tidak untuk dijual kembali/ adanya tangan kedua/ reseller. Dengan kata lain, produk tersebut digunakan langsung oleh konsumen dikarenakan ketahanan produk yang lebih singkat.</p>
    </div>

             <p class="font-visimisi font-size  " style="font-size:22px">4. Produk Pre-order
</p>
<div class="container">

    <p class="visi-font2"> Pre-order atau yang biasa disingkat PO adalah sistem belanja dimana pembeli melakukan pemesanan dan pembayaran dahulu diawal, dengan masa tenggang waktu tunggu (estimasi/perkiraan) yang telah diinfokan dan disepakati antara pembeli dan penjual. Dengan kata lain PO adalah pembeli melakukan pembayaran untuk produk yang akan dibuat karena produk tersebut belum tersedia.</p>
</div>

     <p class="font-visimisi font-size" style="font-size:22px">5. Produk Industri
</p>
<div class="container">

    <p class="visi-font2"> Produk Industri adalah produk yang diproduksi oleh produsen kemudian dibeli oleh konsumen dengan tujuan akan dijual kembali dan juga dipergunakan sebagai bahan baku untuk proses produksi yang akan menghasilkan produk baru yang mempunyai kemanfaatan yang lebih. Dengan kata lain, disebut sebagai produk industri karena harus diolah lagi menjadi produk baru agar dapat dikonsumsi.
        </p>
</div>

   <p class="font-visimisi font-size" style="font-size:22px">6. Kategori Produk
</p>
<div class="container">

<?php 
$n = array("a.","b.","c.","d.","e.","f.","g.","h.","i.","j.","k.","l.","m.","n.","o.","p.");
$isi = array("Tepung dan bahan masak","Bumbu dan rempah","Bahan pokok","Sayur-Sayuran","Buah-buahan",
"Roti dan kue","Produk kesehatan","Minuman (produk industri)","Makanan ringan","Makanan kaleng","Makanan instan
","Makanan beku","Kacang dan bijian","Es krim","Open Pre-order","Makanan dan minuman x-press");
    
foreach($n as $a=>$data){ ?>
<div style="display:flex">
        <p class="visi-font2 mr-1"><?php echo $data ?></p>
    <p class="visi-font2"> <?php echo $isi[$a]."."?></p>
    </div>
<?php } ?>

     

      

</div>
 <p class="font-visimisi font-size" style="font-size:22px">7. Ketentuan produk makanan dan minuman industri rumah tangga
</p>
<div class="container">

<?php
$abjad = array("a.","b.","c.","d.","e.");
$data = array("Produksi produk berasal dari namun tidak terbatas pada restoran vegetarian, rumah makan vegetarian dan atau usaha kecil lainnya.",
"Memenuhi aspek-aspek keamanan pangan, yaitu harus aman, bermutu, bergizi dan terhindar dari bahan kimia dan benda lain yang mengganggu, merugikan dan membahayakan kesehatan.",
"Melakukan praktik-praktik higienis dan sanitasi dalam pengolahan produk, yaitu upaya untuk mengendalikan faktor risiko terjadinya kontaminasi terhadap makanan, baik yang berasal dari bahan makanan, orang, tempat dan peralatan agar aman dikonsumsi.",
"Peralatan masak terbuat dari bahan dan desain alat yang mudah dibersihkan dan tidak boleh melepaskan zat beracun ke dalam bahan pangan (food grade).",
"Produk yang dikemas khusus wajib diberi tanda atau label yang berisi: ");

foreach($abjad as $a=>$abja){
    ?>
    <div style="display:flex">
        <p class="visi-font2 mr-1"><?php echo $abja ?></p>
    <p class="visi-font2"> <?php echo $data[$a]?></p>
    </div>

<?php }?>

<div class="container" style="margin-left:20px;margin-right:20px">

    <?php 
    $ab =array("1.","2.","3.","4.","5.");
    $isi = array("Nama produk;","Daftar bahan yang digunakan;","Berat bersih atau isi bersih;",
                "Nama dan alamat pihak yang memproduksi atau memasukan makanan dan minuman kedalam wilayah Indonesia; dan",
                "Tanggal, bulan dan tahun kadaluwarsa.");

                foreach($ab as $a=>$abjad){ ?>
                      <div style="display:flex">
        <p class="visi-font2 mr-1"><?php echo $abjad ?></p>
    <p class="visi-font2"> <?php echo $isi[$a]?></p>
    </div>

     <?php } ?>
    

</div>

    
</div>
             <div class="garisss" style="margin-top: 20px;"></div>
                     <div class="mt-5">
            <div class="info">
                 <div class="belanjaa">
                   <h2>Kontak Kami</h2>
            <p><a href="" style="pointer-events: none;"><i class="fas fa-phone-square-alt"> 0851-2354-2121(08.00 - 18.00)</i></a></p>
            <p><a href="" style="text-decoration:underline;color:blue"><i style="color: #666"class="fas fa-envelope"></i> klix@gmail.com</a></p>
            
        </div>
<div class="belanjaa">
                <h2>Pembeli</h2>
                <p><a href="">Panduan Belanja</a></p>
                <p><a href="">Pembatalan dan Pengembalian</a></p>
            </div>
            <div class="belanjaa">
                <h2>Penjual</h2>
                <p><a href="">Alasan Jadi Penjual</a></p>
                <p><a href="">Cara Menjadi Penjual</a></p>
                <p><a href="">Ketentuan Produk</a></p>
                <p><a href="">Ketentuan Penjual</a></p>

            </div>

            <div class="belanjaa">
                <h2>Klixaja</h2>
                <p><a href="">Tentang Kami</a></p>
                <p><a href="">Visi & Misi</a></p>
                <p><a href="">Syarat & Ketentuan</a></p>
                <p><a href="">FAQs</a></p>
                <p><a href="">Blog</a></p>
            </div>
            <div class="belanjaa">
                <h2>Download Aplikasi</h2>
                <a href=""><img src="<?php echo base_url('assets/klixaja/images/icons/google_play.png'); ?>" alt=""></a>
                <a href=""><img src="<?php echo base_url('assets/klixaja/images/icons/ios.png'); ?>" alt=""></a>
            </div>
            </div>
        </div>
        
        <!-- garis===================================================================== -->
        
      <div class="garissss" style="margin-top:70px" ></div>

                <div class=" mt-5 d-sm-none d-md-block">
                        <div class="kolom22 mt-5">
                            <div class="pengiriman">
                            <h2>Pengiriman</h2>
                            <img src="<?php echo base_url('assets/klixaja/images/icons/icon_pengiriman.jpeg')?>" alt="">
                            </div>

                             <div class="pembayarann">
                            <h2>Pembayaran</h2>
                            <img src="<?php echo base_url('assets/klixaja/images/icons/icon_pembayaran.jpeg')?>" alt="">
                        </div>
                        </div>

                </div>

            <div class="garisss mt-2" ></div>

                <div class="padding_box_2 wh mt-3">
                    <div class="dis">
                        <p style="margin-left:30px;">&copy 2020 klixaja. All Rights Reserved</p>
                        <!-- <div class="iconv">
                            <p>We Using Safe Payment For</p>
                
                            <img src="assets/klixaja/images/icons/icon_bca.png";?>" alt="">
                            <img style="margin-left:18px;" src="./assets/klixaja/images/icons/icon_mandiri.gif";?>" alt="">
                        </div> -->
                    </div>
                </div>
    </div>
