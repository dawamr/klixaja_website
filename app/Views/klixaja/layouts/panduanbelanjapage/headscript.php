    <link href="<?php echo base_url('assets/klixaja/css/style.css');?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/klixaja/css/cart.css');?>" rel="stylesheet"  type="text/css"  />
    <link rel="stylesheet" href="<?php echo base_url('assets/klixaja/css/footer.css');?>">

    <link href="<?php echo base_url('assets/bootstrap/css/bootstrap.css');?>" rel="stylesheet" />
    <script src="<?php echo base_url('assets/klixaja/js/lib/jquery-3.5.1.min.js')?>"></script>
    <!-- Add icon library -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="<?php echo base_url('assets/klixaja/css/aos.css');?>" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Inika&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Spicy+Rice&display=swap" rel="stylesheet">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="Test the quantity inputs" />
    