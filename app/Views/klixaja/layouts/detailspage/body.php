  <div class="container">
    <div class="row">
       <div class=" col-sm-12 col-md-5">
          <section class="store_gallery" id="gallery">
            <div class="container">
              <div class="row" style="margin-left:0;">
                  <div class="col-md-12" style="padding:0" data-aos="zoom-in">
                    <transition name="slide-fade" mode="out-in">
                      <img
                        :src="photos[activePhoto].url"
                        :key="photos[activePhoto].id"
                        class="w-100 main-image "
                        alt=""
                      />
                    </transition>
                  </div>
                  <div class="col-md-12" >
                      <div class="row" style="margin-left:0;margin:auto;">
                          <div
                            class="col col-md mt-2 " style="padding:0"
                            v-for="(photo,index) in photos"
                            :key="photo.id"
                            data-aos=""
                            data-aos-delay=""
                          >
                            <a href="#" @click="changeActive(index)"
                              ><img
                                :src="photo.url"
                                class="w-100 h-90 thumbnail-image"
                                :class="{active: index== activePhotos}"
                                alt=""
                            /></a>
                          </div>
                      </div>
                   </div>
               </div>
            </div>
          </section>

    </div>

    <div  class=" col-md-7 store_galleryy">
      <div class="container">
        <div class="row no-padding" >
              <div class="col-12">
                    <div class="row " >
                        <h6 class="bg-green text-center uk-label"style="border-radius:5px;padding:5px">Klix Seller</h6>
                        <h5 class=" ml-1 uk-text">Kursi buatan orang Palembang</h5>
                    </div>
              </div>   
              <div class="row align-items-center">
                    <div class="col-1">
                      <a href="">4.8</a>
                    </div>
                    <div class="col-3  no-padding rating sized_box col_auto_max_content_5">
                        <label class="parent star full">&starf;</label>
                        <label class="parent star full">&starf;</label>
                        <label class="parent star full">&starf;</label>
                        <label class="parent star half">&starf;</label>
                        <label class="parent star empty">&starf;</label>    
                    </div>

                    <div class="col-3 tlsn no-padding d-none d-sm-block"><a  href="">100RB</a> Penilaian</div>
                    <div class="col-5 tlsn">200 Terjual</div>
                    

                    <div class="col-12 uk-label">
                    <P style="font-size:22px; display:inline-block;"class="decoration_sale uk-text">Rp70.000</P>
                    <p style="font-size:25px;color:red; display:inline-block;" class="uk-text">Rp60.000</p>
                    <p style="margin-left:10px;background-color:green; color:white; display:inline-block;" >10%OFF</p>
                    <img src="<?php echo base_url('assets/klixaja/images/icons/hijau.png');?>" alt="a" style="margin-left:10px; display:inline-block;height:15px;width:15px;margin-bottom:4px" />
                    <p style="margin-left:1px; color:black; display:inline-block;font-weight:bold;margin-top:5px" >Vegan</p>

                    </div>
                    
                            <!-- <div class="garisssss"></div> -->

                      <div class="info-produk">
                         <div class="col">
                          <p class="font-weight-bold txt-color " >Info <br>Produk</p>
                         </div>
                          <div class="row">
                              <div class="col">
                                <p class="txt-color font-weight-bold">Berat</p>
                                <p style="margin-top:-14px">500gr</p>
                              </div>
                              <div class="col">
                                  <div style="height:40px;border-left: 1px solid rgba(182, 179, 179, 0.527);"></div>
                              </div>
                          </div>
<!--                             
                           <div class="row">
                              <div class="col">
                                <p class="txt-color font-weight-bold mr-2">Kondisi</p>
                                <p style="margin-top:-14px">Baru</p>
                              </div>
                              <div class="col">
                                  <div style="height:40px;border-left: 1px solid rgba(182, 179, 179, 0.527);"></div>
                              </div>
                          </div> -->

                            <div class="row">
                              <div class="col">
                                <p class="txt-color font-weight-bold">Merek</p>
                                <p style="margin-top:-14px">Tidak ada </p>
                              </div>
                              <div class="col">
                                  <div style="height:40px;border-left: 1px solid rgba(182, 179, 179, 0.527);"></div>
                              </div>
                          </div>
                             <div class="row">
                              <div class="col">
                                <p style="margin-left:-30px" class="txt-color font-weight-bold" >Di Kirim dari</p>
                                <p style="margin-top:-14px; margin-left:-30px">Kota Palembang, Sumsel, ID</p>
                              </div>
                             
                          </div>

                      </div>
                          <!-- <div class="garisssss"></div> -->
                        
                      </div>
                    </div>
                   
                                        <!-- <div class="garisss"></div> -->


                <div class="row">
                  <div class="col-2 align-items-center d-none d-sm-block">
                      <p class="font-kuantitas">Kuantitas</p>
                  </div>
                  <div class="col indexxx ">
                      <div class="container">
                        <div class="row align-items-center">
                          <div class="col">
                            <div class="row">
 <span class="stepper">
  <button>–</button>
  <input type="number" id="stepper" value="1" min="1" max="100" step="1">
  <button>+</button>
</span>
                        <h6 style="font-size:13px;margin-left:10px;margin-top:5px">Tersisa 99 buah</h6>
                            </div>
                       

                          </div>

                        </div>
                      </div>
                     
                  </div>
                 

                  <div class="col-md-12 col-sm-12 " style="margin-top:25px">
                  <button id="btnMasukKeranjang" type="button"  style=" margin-right:3px"
                   class="btn btn-default border-dark">Masukkan keranjang</button>
                   <a href="Cart">
                   <button  style=" margin-left:3px" 
                  type="button" class="btn btn-danger w-25">Beli</button>
                  </a>
                </div>
                </div>
                  
        
          </div>
    
    </div>
      </div>
      <div class="container">
        <div class="row 
">
          <div class="col col-sm d-sm-none d-none d-sm-block d-md-block fav-tulisan" style="margin-left:15px">
          <!-- <div href="#" title="Tambahkan ke wishlist" >  <img id="love" style="" src="<?php echo base_url('assets/klixaja/images/icons/heart.svg');?>" alt=""> -->
          <i class="hear_icon icon " style="vertical-align:middle"  title="Tambahkan ke wishlist"></i>
            <h6 class="inline_block" style="">Favorit</h6>
            <h6 class="inline_block">(100)</h6>
            <!-- </div> -->
          </div>
 <div class="col images  d-none d-sm-block d-sm-none d-md-block fav-tulisan" data-toggle="tooltip"  data-placement="bottom" title="Gratis Ongkir Spesial pada semua produk Klix aja dengan min. belanja lebih rendah">
            <img src="<?php echo base_url('assets/klixaja/images/icons/delivery-truck.svg');?>" alt="">
             <h6 class="inline_block">Gratis Ongkir</h6>
          </div>
          
          
          <!-- <div class="col imag  d-none d-sm-block d-sm-none d-md-block fav-tulisan" data-toggle="tooltip"  data-placement="bottom" title="Jaminan 100% Original atau uang kembali 2 kali lipat">
            <img src="<?php echo base_url('assets/klixaja/images/icons/ori.jpg');?>" alt="">
             <h6 class="inline_block">100% Ori</h6>
          </div>

         <div class="col images d-sm-none  d-none d-sm-block d-md-block fav-tulisan" data-toggle="tooltip"  data-placement="bottom" title="Pengembalian gratis* dan praktis s/d 7 hari setelah barang diterima">
            <img src="<?php echo base_url('assets/klixaja/images/icons/returning.svg');?>" alt="">
            <h6 class="inline_block">7</h6>
            <h6 class="inline_block">Hr Pengembalian</h6>
          </div> -->
        </div>
      </div>
      <div class="garisss" style="margin-top: 20px;"></div>
      <div class="container" style="margin-top: 20px;" >
        <div class="row align-items-center">
          <div class="col-2 toko">
            <img class="bulat uk ml-1" src="<?php echo base_url('assets/klixaja/images/icons/store.svg');?>" alt="">
            <h6 class="bg-green text-center" style="width:80px; border-radius:5px;position:relative; margin-top:-12px">Klix Seller</h6>
          </div>
          <div class="col uk-text" style="margin-left:20px; ">
              <h6>IndoShop</h6>
              <h6 class="txt-color">aktif 20 menit lalu</h6>
              <div class="row r  ukuran-btn ">
                <button type="button"  class="btn btn-success mr-3 btn-uk mt-1" style="display:flex;align-items:center">Chat Sekarang</button>
                <button type="button" onclick="location.href='Toko'" class="btn btn-default mt-1 btn-light" style="display:flex;align-items:center">Kunjungi Toko</button>
              </div>
              
          </div>

          <div class="col-6  d-none d-lg-block">
                <!-- <div class="row">
                  <div style="height:90px;width:1px;background:black; display:inline-block"></>
                  </div> -->
            <div class="col-12">
              <div class="row uk-text" >
                <h6 class="txt-color" >Penilaian</h6>
                <h6 style="margin-left:5px">100RB</h6>
                <div class="row uk-text ml-4">
                <h6  class="txt-color" style="display:inline-block; margin-right:5px">Bergabung</h6>
                <h6>18 bulan lalu</h6>
              </div>
              </div>
              
            </div>
            <div class="col-12">

            </div>
            <div class="row uk-text" >
              <h6 class="txt-color ml-3" >Produk</h6>
              <h6 class="ml-3">120</h6>
              <div class="row ml-5">
                 <h6 class=" txt-color" style="margin-left:-8px">Pengikut</h6>
                 <h6 class="ml-3">120RB</h6>
              </div> 
            </div>
          </div>
        </div>
      </div>
      <div class="garisss" style="margin-top: 10px; margin-bottom:20px"></div>

      <div class="container" >
      <div class="row" id="kepala">
            <div class="col">
              <div  class=" row hoverrr align-items-center" style="margin:0;padding:0">
                <a class=" "href="#infoproduk" data-scroll="deskripsi" style="text-decoration:none">
                <div class="col moven">
                   <p id="des" class="font-deskripsi ppp pa " >Deskripsi</p>
                </div>
                </a>
                <a class=""href="#ulasan" data-scroll="ulasan" style="text-decoration:none">
                   <div class="col moven">
                      <p id="ula" class="font-deskripsi paa">Ulasan(197)</p>
                   </div>
                </a>
               <a class=""href="#diskusi" data-scroll="diskusi" style="text-decoration:none">
                <div class="col moven">
                  <p id="dis"class="font-deskripsi " >Diskusi(200)</p>
                </div>
               </a>
                <a class=""href="#rekomendasi" data-scroll="rekomendasi" style="text-decoration:none">
                  <div class="col moven">
                   <p id="rek" class="font-deskripsi pa">Rekomendasi</p>
                  </div>
               </a>
              </div>
        </div>
        
            <button type="button" class="btn btn-laporkan mr-2 font-deskripsi lebar-button">Laporkan Produk</button>
            <button type="button" class="btn btn-laporkan font-deskripsi lebar-button">Share</button>     

      </div>
     </div>
     <div class="lines">
            <div class="garisss" style="margin-top: 10px;"></div>

     </div>
      <div class="container mt-3">
        <h5 id="infoproduk" class="font-weight-bold mt-2 d-xl-none" >Informasi Produk</h5>
        <div class="">
          <div class="row d-xl-none">
          <div class="col-2 ">
              <div class=" fav-tulisannn">
                  <h6 class="txt-color ">Kategori</h6>
                  <h6 class="txt-color">Berat</h6>
                  <h6 class="txt-color ">Merek</h6>
                  <h6 class="txt-color " >Kondisi</h6>
                  <h6 class="txt-color ">Dikirim Dari</h6>
              </div>
           </div>
           <div class="col fav-tulisannn">
             <div class="row">
                <h6><a  style="text-decoration:none; margin-left:14px" href="">Daging vege</a>></h6>
                <h6><a style="text-decoration:none" href="">rendang</a>></h6>
                <h6><a style="text-decoration:none" href="">beku</a></h6>
             </div>
                <h6 >500gr</h6>
                <h6><a  style="text-decoration:none;" href="">Tidak Ada Merek</a> </h6>
                <h6 >Baru</h6>
                <h6>Kota Palembang, Sumsel, ID</h6>
           </div>
          
          </div>
         
        </div>

        <h4 id="deskripsi" data-anchor="deskripsi" class="font-weight-bold mt-3">Deskripsi Produk</h4>
        <p class="fav-tulisannnn">
          ⭐️ DISABLON!!!! <br>
          ⭐️BARANG YANG ADA DI FOTO ADALAH BARANG YANG DIJUAL <br>
          ⭐️ MOHON PADA SAAT ORDER TOLONG CANTUMKAN WARNA DICATATAN ORDERAN YANG DI INGINKAN DAN WARNA CADANGAN SEBAGAI ALTERNATIF JIKA BARANG YANG DIPESAN KOSONG, JIKA TIDAK ADA MAKA DIKIRIM RANDOM :) <br>
            CONTOH: WARNA MAROON (CADANGAN NAVY, HIJAU LUMUT, HITAM) <br>
          ⭐️Kualitas BAHAN:Cotton Fleece > DIJAMIN Tebal, Halus, dan Gak bikin gerah <br>
          ⭐️ SAAT CUACA DINGIN JUGA SANGAT COCOK DIPAKAI KARENA SWEATER INI AKAN MEMBUAT BADAN TERASA HANGAT <br>
          ⭐️ (PREMIUM QUALITY GUARANTEED!) <br>
          ⭐️READY STOCK <br>
          ⭐️Jahitan SUPER RAPI <br>
          ⭐️ ALL REAL PICTURE 100 % (FOTO DIAMBIL SENDIRI) <br>
          ⭐️Harga dijamin Termurah ! <br>
          ⭐️Kualitas STANDARD Ekspor ! <br>
          ⭐️Reseller Welcome, <br>
          ⭐️JAKET UNISEX (BISA PRIA DAN WANITA) <br>
        </p>


        <h4 id="ulasan" data-anchor="ulasan" class="mt-5">Ulasan Produk</h4>
           <h6 class="float-right d-block d-sm-none lihat-semua" style="font-size:12px">Lihat semua</h6>

        <div class="container">
              <div class="row">
                <div class="col col-xs-12 mr-5" style="padding:0">
                    <div class="row d-none d-sm-block align-items-center" style="margin-left:0">
                      <h3 style="margin-right:5px">4.6</h3>
                      <h6>dari 5</h6>
                    </div>
                  
                    <div class="container">

                    <div class="row align-items-center ">             
                      <div class="mleft rating sized_box col_auto_max_content_5">
                        <label class="parent star full">&starf;</label>
                        <label class="parent star full">&starf;</label>
                        <label class="parent star full">&starf;</label>
                        <label class="parent star half">&starf;</label>
                        <label class="parent star empty">&starf;</label>
                      </div>
                      <h6 class="ml-1 d-block d-sm-none" style="font-size:12px">4.6/5 (100k ulasan)</h6>
                    </div>
                      </div>
            
                </div>

                <div class="col-10 d-none d-sm-block">
                  <div id="hovv" class="row s">
                      <button  class="btn btn-default border-dark mr-2 mt-2 aktif">Semua</button>
                      <button  class="btn btn-default border-dark mr-2 mt-2">5 Bintang (233)</button>
                      <button  class="btn btn-default border-dark mr-2 mt-2">4 Bintang (31)</button>
                      <button  class="btn btn-default border-dark mr-2 mt-2">3 Bintang (64)</button>
                      <button  class="btn btn-default border-dark mr-2 mt-2">2 Bintang (3)</button>
                      <button  class="btn btn-default border-dark mr-2 mt-2">1 Bintang (0)</button>
                      <button  class="btn btn-default border-dark mr-2 mt-2">Dengan Komentar (105)</button>
                      <button  class="btn btn-default border-dark mt-2">Dengan Media (100)</button>
                  </div>
                
                </div>
              </div>
              
              <div class="row mt-4">
                    <div class="gmbr">
                      <img class="bulatt" src="<?php echo base_url('assets/klixaja/images/banner1.jpeg');?>" alt="">
                    </div>
                    <div class="col fav-tulisan">
                      <h6>Alexander</h6>
                      <div class="rating sized_box col_auto_max_content_5">
                        <label class="parent star full">&starf;</label>
                        <label class="parent star full">&starf;</label>
                        <label class="parent star full">&starf;</label>
                        <label class="parent star half">&starf;</label>
                        <label class="parent star empty">&starf;</label>
                      </div>
                      <h6>Pengiriman cepat, kualitas produk sangat bagus, toko ini sangat rekomended</h6>
                      <div class="col-4 gmbar">
                      <img class="mb-1" src="<?php echo base_url('assets/klixaja/images/banner1.jpeg');?>" alt="">
                      <img  src="<?php echo base_url('assets/klixaja/images/banner1.jpeg');?>" alt="">
                      </div>
                      <h6>2020-08-4 06:20</h6>
                      <div class="row align-items-center">
                      <img style="height:15px; width:15px; margin-right:4px;margin-left:5px"src="<?php echo base_url('assets/klixaja/images/icons/like.svg');?>" alt="">
                      <h6>233</h6>
                      </div>


                    </div>
              </div>
                    <div class="garisss" style="margin-top: 5px;"></div>

              <div class="row mt-4">
                    <div class="gmbr">
                      <img class="bulatt" src="<?php echo base_url('assets/klixaja/images/banner1.jpeg');?>" alt="">
                    </div>
                    <div class="col fav-tulisan">
                      <h6>Alex</h6>
                      <div class="rating sized_box col_auto_max_content_5">
                        <label class="parent star full">&starf;</label>
                        <label class="parent star full">&starf;</label>
                        <label class="parent star full">&starf;</label>
                        <label class="parent star half">&starf;</label>
                        <label class="parent star empty">&starf;</label>
                      </div>
                      <h6>Pengiriman cepat, kualitas produk sangat bagus, toko ini sangat rekomended</h6>
                      <div class="col-4 gmbar">
                      <img class="mb-1" src="<?php echo base_url('assets/klixaja/images/banner1.jpeg');?>" alt="">
                      <img  src="<?php echo base_url('assets/klixaja/images/banner1.jpeg');?>" alt="">
                      </div>
                      <h6>2020-08-4 06:20</h6>
                      <div class="row align-items-center">
                      <img style="height:15px; width:15px; margin-right:4px;margin-left:5px"src="<?php echo base_url('assets/klixaja/images/icons/like.svg');?>" alt="">
                      <h6>233</h6>
                      </div>


                    </div>
              </div>


               <div class="garisss" style="margin-top: 5px;"></div>

              <div class="row mt-4">
                    <div class="gmbr">
                      <img class="bulatt" src="<?php echo base_url('assets/klixaja/images/banner1.jpeg');?>" alt="">
                    </div>
                    <div class="col fav-tulisan">
                      <h6>Beni</h6>
                      <div class="rating sized_box col_auto_max_content_5">
                        <label class="parent star full">&starf;</label>
                        <label class="parent star full">&starf;</label>
                        <label class="parent star full">&starf;</label>
                        <label class="parent star half">&starf;</label>
                        <label class="parent star empty">&starf;</label>
                      </div>
                      <h6>Pengiriman cepat, kualitas produk sangat bagus, toko ini sangat rekomended</h6>
                      <div class="col-4 gmbar">
                      <img class="mb-1" src="<?php echo base_url('assets/klixaja/images/banner1.jpeg');?>" alt="">
                      <img  src="<?php echo base_url('assets/klixaja/images/banner1.jpeg');?>" alt="">
                      </div>
                      <h6>2020-08-4 06:20</h6>
                      <div class="row align-items-center">
                      <img style="height:15px; width:15px; margin-right:4px;margin-left:5px"src="<?php echo base_url('assets/klixaja/images/icons/like.svg');?>" alt="">
                      <h6>233</h6>
                      </div>
                    </div>
               </div>
        </div>
        <nav>
          <ul class="pagination float-right">
            <li class="page-item disabled">
                <a class="page-link" href="#">Sebelumnya</a>
            </li>
            <li class="page-item active  indexxx"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#">4</a></li>
            <li class="page-item"><a class="page-link" href="#">5</a></li>
            <li class="page-item">
              <a class="page-link" href="#">Selanjutnya</a>
            </li>
          </ul>
        </nav>


         <!-- <div class="bg mt-5" data-aos="fade-up">
                        <div class="detail">
                        <h4 style="color: #000;">Produk Lain dari Toko</h4>

                        </div>
                    </div> -->

                   
                   
      </div>

      <div class="container">
      <h4 id="diskusi" data-anchor="diskusi" class="mt-5">Diskusi(378)</h4>

        <div class="gtc  ">
          <div class="row"> 
            <img class="qa-gambar" src="<?php echo base_url('assets/klixaja/images/icons/qa.svg');?>" style="width:30px;height:30px;margin-left:30px;margin-right:5px" alt="">
            <p class="ukuran">Ada Pertanyaan? Ayo Diskusikan</p>
          </div>
          <div class="col">
          <button type="button " style="font-size:12px" class="btn btn-success   butn">Tulis Pertanyaan</button>
            
          </div>
        </div>

          <div class="lebar mt-3  ">
            <div class="row ml-1">
                <div class="gambarrr">  
                      <img class="bulatt" src="<?php echo base_url('assets/klixaja/images/banner1.jpeg');?>" alt="">
                    </div>
                    <div class="col fav-tulisan">
                      <div class="row">
                      <h6 class="mr-2 ml-3 font-weight-bold">Beni</h6>
                      <h6>2020-08-4 06:10</h6>
                      </div>
                      <h6>Apakah Barangnya ready kak?</h6>
                     </div>
              </div>
              <div class="garisss" style="margin-top: 0px;"></div>
            <div class="row ml-3 mt-3">
                <div class="gambarrr">  
                      <img class="bulatt" src="<?php echo base_url('assets/klixaja/images/icons/store.svg');?>" alt="">
                    </div>
                    <div class="col fav-tulisan">
                      <div class="row">
                      <h6 class="mr-2 ml-3 font-weight-bold">IndoShop</h6>
                      <h6 style="background:#86f10b " class=" mr-2 ">Penjual</h6>
                      <h6>2020-08-4 06:10</h6>
                      </div>
                      <h6>Ready kak, Silahkan diorder yah :)</h6>
                     </div>
              </div>

                <div class="row ml-3 mt-3">
                <div class="gambarrr">  
                      <img class="bulatt" src="<?php echo base_url('assets/klixaja/images/preorder1.jpg');?>" alt="">
                    </div>
                    <div class="col">
                     <div class="input-group">
                    <input type="text" style="z-index:0;"class="form-control" placeholder="Isi Komentar disini" aria-label="Isi Komentar disini" aria-describedby="basic-addon2">
                  
                  </div>
                    <div class="input-group-append">
                       <button style="width:60px; height:25px;font-size:11px;z-index:0" type="button" class="btn btn-success mt-1">Kirim</button>
                        <button style="width:60px; height:25px;font-size:11px;z-index:0;" type="button" class="btn btn-success ml-1 mt-1">Batal</button>

                    </div>
                    </div>
                   
              
          </div>
                    
          </div>

             <div class="lebar mt-3  ">
            <div class="row ml-1">
                <div class="gambarrr">  
                      <img class="bulatt" src="<?php echo base_url('assets/klixaja/images/banner1.jpeg');?>" alt="">
                    </div>
                    <div class="col fav-tulisan">
                      <div class="row">
                      <h6 class="mr-2 ml-3 font-weight-bold">Beni</h6>
                      <h6>2020-08-4 06:10</h6>
                      </div>
                      <h6>Apakah Barangnya ready kak?</h6>
                     </div>
              </div>
              <div class="garisss" style="margin-top: 0px;"></div>
            <div class="row ml-3 mt-3">
                <div class="gambarrr">  
                      <img class="bulatt" src="<?php echo base_url('assets/klixaja/images/icons/store.svg');?>" alt="">
                    </div>
                    <div class="col fav-tulisan">
                      <div class="row">
                      <h6 class="mr-2 ml-3 font-weight-bold">IndoShop</h6>
                      <h6 style="background:#86f10b " class=" mr-2 ">Penjual</h6>
                      <h6>2020-08-4 06:10</h6>
                      </div>
                      <h6>Ready kak, Silahkan diorder yah :)</h6>
                     </div>
              </div>

                <div class="row ml-3 mt-3">
                <div class="gambarrr">  
                      <img class="bulatt" src="<?php echo base_url('assets/klixaja/images/preorder1.jpg');?>" alt="">
                    </div>
                    <div class="col">
                     <div class="input-group">
                    <input type="text" style="z-index:0;" class="form-control" placeholder="Isi Komentar disini" aria-label="Isi Komentar disini" aria-describedby="basic-addon2">
                 
                  </div>
                     <div class="input-group-append">
                     <button style="width:60px; height:25px;font-size:11px;z-index:0" type="button" class="btn btn-success mt-1">Kirim</button>
                        <button style="width:60px; height:25px;font-size:11px;z-index:0;" type="button" class="btn btn-success ml-1 mt-1">Batal</button>
                    </div>
                    </div>
          </div>
                    
          </div>

          <div class="form-group mt-5">
  <textarea class="form-control rounded-0" id="exampleFormControlTextarea2" rows="5" placeholder="Apa yang ingin Anda tanyakan?"></textarea>
</div>
          <button type="button" class="btn btn-success mr-3">Kirim</button>
          

    

      <div class="container">
         <div style="margin-top: 40px;" class="bg" data-aos="fade-up">
                        <div class="detail" >
                        <h4 id="rekomendasi" data-anchor="rekomendasi" style="color: #000;" class="font-view">Produk Lain dari Toko</h4>
                        <p class=""><a href="">View All</a> </p>
                        </div>
                    </div>

        <!-- <div data-aos="fade-up" class="row ">
          <h4 style="margin-left:13px" class="">Produk Lain dari Toko</h4>
          <p class="text-right">View All</p>
        </div> -->

           <div class="contain">
              <?php
              for($i=1;$i<11;$i++){
                  $rand = rand(1,5);
                  ?>
              <div data-aos="fade-up" data-aos-delay=<?php echo $i."00"; ?>>
              
              <div class="bentukk"


              >
                 <div style="font-size:12px" class="decoration_label_box posisi_absolute"><span>Star Seller</span></div>
                            <div class="decoration_label_box decoration_label_box_2 posisi_absolute" style="height:35px;right:0;margin-right:4px"><span>33%</span><span>Off</span></div>
              <a style="text-decoration:none" href="Details">
              <img src="<?php echo base_url('assets/klixaja/images/preorder1.jpg')?>" alt=""> 
              </a>
              <!-- <div class="tombol" >
                  <div class="desain_button" >
                  <a href=""><button class="bg_button_addtocart"type="button">Add to cart</button></a>  <br>
                  <a href=""><button class="bg_button_quickview" type="button">Quick View</button></a> 
                  </div>
              </div> -->
              
                      <a href="Details" style="text-decoration:none">
                <div class="max_line">
              <p class="font_judull hide-text">Martabak Spesial vegetarian asal palembang</p>
              </div> 
              </a>
              <div class="image_desc " style="margin-top:-20px;">
                      <div class="image_desc_detail">
                          <i class="heart_icon icon " title="Tambahkan ke wishlist"></i>
                      <a href="details.php" style="text-decoration:none">
                          <div class="rating sized_box col_auto_max_content_5">
                              <label class="parent star full">&starf;</label>
                              <label class="parent star full">&starf;</label>
                              <label class="parent star full">&starf;</label>
                              <label class="parent star half">&starf;</label>
                              <label class="parent star empty">&starf;</label>
                          </div>
                        </a>
                      </div>
                      <a href="Details" style="text-decoration:none">
                      <div class="hargaa" style="">
                          <p class=" hrg decoration_sale" style="float:left;margin-right:10px">Rp1.250.000</p>
                          <p class="hrgg decoration_discount" style="">Rp1.000.000</p>
                          <!-- grid,gap=10px,  grid-template-columns: max-content max-content; -->

                      </div>
                      </a>
                  </div>
                  
              </div>
              </div>
           <?php } ?>
          </div>



         <div style="margin-top: 20px;" class="bg" data-aos="fade-up">
                        <div class="detail">
                        <h5 class="font-vieww" style="color: #000;">Produk Serupa</h5>
                        <p><a href="">View All</a> </p>
                        </div>
                    </div>
            <div class="contain">
              <?php
              for($i=1;$i<11;$i++){
                  $rand = rand(1,5);
                  ?>
              <div data-aos="fade-up" data-aos-delay=<?php echo $i."00"; ?>>
              
              <div class="bentukk"


              >
                <div style="font-size:12px" class="decoration_label_box posisi_absolute"><span>Star Seller</span></div>
                            <div class="decoration_label_box decoration_label_box_2 posisi_absolute" style="height:35px;right:0;margin-right:4px">
                            <span>33%</span><span>Off</span></div>
              <a style="text-decoration:none" href="details.php">
              <img src="<?php echo base_url('assets/klixaja/images/preorder1.jpg');?>" alt=""> 
              </a>
              <!-- <div class="tombol" >
                  <div class="desain_button" >
                  <a href=""><button class="bg_button_addtocart"type="button">Add to cart</button></a>  <br>
                  <a href=""><button class="bg_button_quickview" type="button">Quick View</button></a> 
                  </div>
              </div> -->
                
                      <a href="Details" style="text-decoration:none">
                <div class="max_line">
              <p class="font_judull hide-text" 
              >Martabak Spesial vegetarian asal palembang</p>
              </div> 
              </a>
              <div class="image_desc " style="margin-top:-20px;">
                      <div class="image_desc_detail">
                          <i class="heart_icon icon " title="Tambahkan ke wishlist"></i>
                      <a href="Details" style="text-decoration:none">
                          <div class="rating sized_box col_auto_max_content_5">
                              <label class="parent star full">&starf;</label>
                              <label class="parent star full">&starf;</label>
                              <label class="parent star full">&starf;</label>
                              <label class="parent star half">&starf;</label>
                              <label class="parent star empty">&starf;</label>
                          </div>
                        </a>
                      </div>
                      <a href="Details" style="text-decoration:none">
                      <div class="hargaa" style="">
                          <p class=" hrg decoration_sale" style="float:left;margin-right:10px">Rp1.250.000</p>
                          <p class="hrgg decoration_discount" style="">Rp1.000.000</p>
                          <!-- grid,gap=10px,  grid-template-columns: max-content max-content; -->

                      </div>
                      </a>
                  </div>
                  
              </div>
              </div>
           <?php } ?>
          </div>
                    <div class="garisss" style="margin-top: 20px;"></div>
                     <div class="mt-5">
            <div class="info">
                <div class="belanjaa">
                   <h2>Kontak Kami</h2>
            <p><a href="" style="pointer-events: none;"><i class="fas fa-phone-square-alt"> 0851-2354-2121(08.00 - 18.00)</i></a></p>
            <p><a href="" style="text-decoration:underline;color:blue"><i style="color: #666"class="fas fa-envelope"></i> klix@gmail.com</a></p>
            
        </div>
<div class="belanjaa">
                <h2>Pembeli</h2>
                <p><a href="">Panduan Belanja</a></p>
                <p><a href="">Pembatalan dan Pengembalian</a></p>
            </div>
            <div class="belanjaa">
                <h2>Penjual</h2>
                <p><a href="">Alasan Jadi Penjual</a></p>
                <p><a href="">Cara Menjadi Penjual</a></p>
                <p><a href="">Ketentuan Produk</a></p>
                <p><a href="">Ketentuan Penjual</a></p>

            </div>

            <div class="belanjaa">
                <h2>Klixaja</h2>
                <p><a href="">Tentang Kami</a></p>
                <p><a href="">Visi & Misi</a></p>
                <p><a href="">Syarat & Ketentuan</a></p>
                <p><a href="">FAQs</a></p>
                <p><a href="">Blog</a></p>
            </div>
            <div class="belanjaa">
                <h2>Download Aplikasi</h2>
                <a href=""><img src="<?php echo base_url('assets/klixaja/images/icons/google_play.png'); ?>" alt=""></a>
                <a href=""><img src="<?php echo base_url('assets/klixaja/images/icons/ios.png'); ?>" alt=""></a>
            </div>
            </div>
        </div>
        
        <!-- garis===================================================================== -->
        
      <div class="garissss" style="margin-top:70px" ></div>

                <div class=" mt-5 d-sm-none d-md-block">
                        <div class="kolom22 mt-5">
                            <div class="pengiriman">
                            <h2>Pengiriman</h2>
                            <img src="<?php echo base_url('assets/klixaja/images/icons/icon_pengiriman.jpeg');?>" alt="">
                            </div>

                             <div class="pembayarann">
                            <h2>Pembayaran</h2>
                            <img src="<?php echo base_url('assets/klixaja/images/icons/icon_pembayaran.jpeg');?>" alt="">
                        </div>
                        </div>

                </div>

            <div class="garisss mt-2" ></div>

                <div class="padding_box_2 wh mt-3">
                    <div class="dis">
                        <p style="margin-left:30px;">&copy 2020 klixaja. All Rights Reserved</p>

                        <!-- <div class="iconv">
                            <p>We Using Safe Payment For</p>
                
                            <img src="assets/klixaja/images/icons/icon_bca.png";?>" alt="">
                            <img style="margin-left:18px;" src="assets/klixaja/images/icons/icon_mandiri.gif";?>" alt="">
                        </div> -->
                    </div>
                </div>

      </div>
      <script>
var inc = document.getElementsByClassName("stepper");
for (i = 0; i < inc.length; i++) {
  var incI = inc[i].querySelector("input"),
    id = incI.getAttribute("id"),
    min = incI.getAttribute("min"),
    max = incI.getAttribute("max"),
    step = incI.getAttribute("step");
  document
    .getElementById(id)
    .previousElementSibling.setAttribute(
      "onclick",
      "stepperInput('" + id + "', -" + step + ", " + min + ")"
    ); 
  document
    .getElementById(id)
    .nextElementSibling.setAttribute(
      "onclick",
      "stepperInput('" + id + "', " + step + ", " + max + ")"
    ); 
}

function stepperInput(id, s, m) {
  var el = document.getElementById(id);
  if (s > 0) {
    if (parseInt(el.value) < m) {
      el.value = parseInt(el.value) + s;
    }
  } else {
    if (parseInt(el.value) > m) {
      el.value = parseInt(el.value) + s;
    }
  }
}</script>


<script>
// Add active class to the current button (highlight it)
var header = document.getElementById("hovv");
var btns = header.getElementsByClassName("btn");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
  var current = document.getElementsByClassName("aktif");
  current[0].className = current[0].className.replace(" aktif", "");
  this.className += " aktif";
  });
}


//untuk sticky header
window.onscroll = function(){
  mySticky();
};
var header = document.querySelector('div#kepala');
var sticky = header.offsetTop;
// var button = document.getElementsByClassName('btn-laporkan')
var button = document.querySelectorAll('div#kepala button');
var marginTop = document.querySelectorAll('div#kepala .col .row a .col p');
 
var ulasan = document.getElementById('ulasan');
var diskusi = document.getElementById('diskusi');
var rekomendasi = document.getElementById('rekomendasi');

var des = document.getElementById('des');
// var de = document.querySelector('div#kepala .col-8 .row a .col-2 p')[1];

  // des.addEventListener("click", function(){
  //   var deskripsi = document.getElementById('deskripsi');
  //     deskripsi.scrollIntoView();
  // });

function mySticky(){
  if(window.pageYOffset > sticky){
    //menambah class baru
    // header.classList.add("stickyy");

     if(window.innerWidth < 516){
         header.classList.remove("stickyy");
    }
    if(window.innerWidth >=516){
         header.classList.add("stickyy"); 
          for(i=0;i<button.length;i++){
    button[i].style.display ='none';
    } 
    }
   

    for(var i=0;i<marginTop.length;i++){
    
    marginTop[i].classList.add("mrgn");


//     marginTop[i].addEventListener("click", function() {
//   var current = document.getElementsByClassName("ppp");
//   current[0].className = current[0].className.replace("ppp", "");
//   this.className += "ppp";
  
// });

    }
  }else{
    //hapus class
    header.classList.remove("stickyy");
  for(i=0;i<button.length;i++){
    button[i].style.display ='block';  

    }
    for(i=0;i<marginTop.length;i++){
    marginTop[i].classList.remove("mrgn");
    }
  }
}




// //jquery =============================================================
// $('.hoverrr a').on('click', function() {

//     var scrollAnchor = $(this).attr('data-scroll'),
//         scrollPoint = $('h4[data-anchor="' + scrollAnchor + '"]').offset().top - 28;

//     $('body,html').animate({
//         scrollTop: scrollPoint
//     }, 500);

//     return false;

// })

// $(window).scroll(function() {
//     var windscroll = $(window).scrollTop();
//     if (windscroll >= 100) {
//         $('nav').addClass('fixed');
//         $('.wrapper section').each(function(i) {
//             if ($(this).position().top <= windscroll - 20) {
//                 $('nav a.active').removeClass('active');
//                 $('nav a').eq(i).addClass('active');
//             }
//         });

//     } else {

//         $('nav').removeClass('fixed');
//         $('nav a.active').removeClass('active');
//         $('nav a:first').addClass('active');
//     }

// }).scroll();

</script>


     <!-- untuk pake vue -->
    <script src="<?php echo base_url('assets/vue/vue.js')?>"></script>
    <script>
      var gallery = new Vue({
        el: "#gallery",
        mounted() {
          AOS.init();
        },
        data: {
          activePhoto: 0,
          photos: [
            {
              id: 0,
              url: "<?php echo base_url('assets/klixaja/images/product-thumbnail-1.jpg')?>",
            },
            {
              id: 1,
              url: "<?php echo base_url('assets/klixaja/images/product-thumbnail-2.jpg')?>",
            },
            {
              id: 2,
              url: "<?php echo base_url('assets/klixaja/images/product-thumbnail-3.jpg')?>",
            },
            {
              id: 3,
              url: "<?php echo base_url('assets/klixaja/images/product-thumbnail-4.jpg')?>",
            },
             {
              id: 4,
              url: "<?php echo base_url('assets/klixaja/images/product-thumbnail-4.jpg')?>",
            }
          ],
        },

        methods: {
          changeActive(id) {
            this.activePhoto = id;
          },
        },
      });
    </script>


 <script src="<?php echo base_url('assets/bootstrap/js/jquery-3.5.1.slim.min.js');?>"></script>
<script src="<?php echo base_url('https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.min.js')?>">
</script>

<script>
const a= document.querySelectorAll('a .moven p');
  $('.hoverrr a .moven p').on('click', function(){
            $(".moven p").removeClass('ppp');
    // $('div a div p').removeClass('ppp');
    $(this).addClass('ppp');
});
</script>

    <script>
          $(".heart_icon").on("click", function () {
    if ($(this).hasClass("fill")) {
      $(this).removeClass("fill");
    } else {
      $(this).addClass("fill");
    }
  });
    </script>

      <script>
          $(".hear_icon").on("click", function () {
    if ($(this).hasClass("fill")) {
      $(this).removeClass("fill");
    } else {
      $(this).addClass("fill");

    }
  });
    </script>

<script>
  $('#btnMasukKeranjang').on('click', function(){
    Swal.fire({
  title: 'Produk Berhasil ditambahkan ke Keranjang',
  type: 'success',
  showCloseButton: true,
         customClass: 'swal-wide',

  
})



  });
</script>
</div>
  