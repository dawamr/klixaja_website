<div class=" container containe mtop"  >
    <div class=""  >
        <h3 class="text-visi1">Alasan Berjualan di Klixaja</h3>
    </div>

 
    <div class="row align-items-center mt-4" data-aos="fade-right" >
        <div style="padding-right:0"class="col-lg-3 col-md-4 col-sm-3 col-3" data-aos="zoom-in-down">
            <img class="img-visilogo" style="width:200px;height:60px" src="<?php echo base_url('assets/klixaja/images/jadipenjual1.png')?>" alt="">
        </div>
        <div style="" class="my-auto col-lg-9 col-md-8 col-sm-9 col-9">
            <p class="visi-font">
              E-Commerce pertama dengan Produk Sehat Nabati Terlengkap dan Terverifikasi IVS.
            </p>
        </div>
    </div>

      <div class="row mt-2"  data-aos="fade-left">
        <div class="my-auto col-lg-9 col-md-8 col-sm-9 col-9">
            <div class="font-visimisi font-jadipenjual1" >Potensi Pasar Luas</div>
            <p class="visi-font1">
             Kesadaran masyarakat tentang pola hidup sehat membuat permintaan masyarakat terhadap produk sehat nabati semakin meningkat.
            </p>
        </div>
         <div class="col-lg-3 col-md-4 col-sm-3 col-3"  data-aos="zoom-in-down">
            <img class="img-visi1" src="<?php echo base_url('assets/klixaja/images/jadipenjual2.jpg')?>" alt="">
        </div>
    </div>

<div class="row mt-4 align-items-center" data-aos="fade-right">
        <div class="col-lg-3 col-md-4 col-sm-3 col-3" data-aos="zoom-in-down">
            <img class="img-visi" src="<?php echo base_url('assets/klixaja/images/jadipenjual3.png')?>" alt="">
        </div>
        <div class="my-auto col-lg-9 col-md-8 col-sm-9 col-9">

                        <div class="font-visimisi font-jadipenjual" >Produk Makanan dan Minuman X-press</div>

            <p class="visi-font">
Klixaja mendukung penjualan makanan dan minuman X-press dimana pembeli bisa langsung memesan makanan dan minuman X-press yang dapat dikirim langsung oleh penjual hari itu juga dengan jasa kirim yang tersedia.        </p>
        </div>
</div>


    <div class="row  mt-4"  data-aos="fade-left">
        <div class="my-auto col-lg-9 col-md-8 col-sm-9 col-9">
            <div class="font-visimisi font-jadipenjual1" >Produk Pre-order</div>
            <p class="visi-font1">
Klixaja juga mendukung penjualan produk Pre-order yang mengharuskan pembeli memesan dan membayar terlebih dahulu dan pesanan akan diproses keesokan harinya sesuai kesepakatan antara pembeli dan penjual.        </div>
         <div class="col-lg-3 col-md-4 col-sm-3 col-3"  data-aos="zoom-in-down">
            <img class="img-visi1" src="<?php echo base_url('assets/klixaja/images/jadipenjual4.jpg')?>" alt="">
        </div>
    </div>


<div class="row mt-4 align-items-center" data-aos="fade-right">
        <div class="col-lg-3 col-md-4 col-sm-3 col-3" data-aos="zoom-in-down">
            <img class="img-visi" src="<?php echo base_url('assets/klixaja/images/jadipenjual5.jpg')?>" alt="">
        </div>
        <div class="my-auto col-lg-9 col-md-8 col-sm-9 col-9">
                        <div class="font-visimisi font-jadipenjual" >Pengaturan dan penampilan toko yang mudah diatur</div>
            <p class="visi-font">
Anda bisa mengatur etalase toko anda sesuai produk yang anda jual agar memudahkan pembeli menemukan produk yang anda jual. Selain itu Klixaja juga menyediakan Pilihan fitur-fitur yang dapat mengoptimalkan penjualan tokomu.            </p>
        </div>
</div>

 <div class="row mt-4"  data-aos="fade-left">
        <div class="my-auto     col-lg-9 col-md-8 col-sm-9 col-9">
            <p class="visi-font1">
Jasa Kirim Aman & Terpercaya.        </div>
         <div class="col-lg-3 col-md-4 col-sm-3 col-3"  data-aos="zoom-in-down">
            <img class="img-visi1" src="<?php echo base_url('assets/klixaja/images/jadipenjual6.png')?>" alt="">
        </div>
    </div>

       <div class="garisss" style="margin-top: 20px;"></div>
                     <div class="mt-5">
            <div class="info">
                <div class="belanjaa">
                   <h2>Kontak Kami</h2>
            <p><a href="" style="pointer-events: none;"><i class="fas fa-phone-square-alt"> 0851-2354-2121(08.00 - 18.00)</i></a></p>
            <p><a href="" style="text-decoration:underline;color:blue"><i style="color: #666"class="fas fa-envelope"></i> klix@gmail.com</a></p>
            
        </div>
<div class="belanjaa">
                <h2>Pembeli</h2>
                <p><a href="">Panduan Belanja</a></p>
                <p><a href="">Pembatalan dan Pengembalian</a></p>
            </div>
            <div class="belanjaa">
                <h2>Penjual</h2>
                <p><a href="">Alasan Jadi Penjual</a></p>
                <p><a href="">Cara Menjadi Penjual</a></p>
                <p><a href="">Ketentuan Produk</a></p>
                <p><a href="">Ketentuan Penjual</a></p>

            </div>

            <div class="belanjaa">
                <h2>Klixaja</h2>
                <p><a href="">Tentang Kami</a></p>
                <p><a href="">Visi & Misi</a></p>
                <p><a href="">Syarat & Ketentuan</a></p>
                <p><a href="">FAQs</a></p>
                <p><a href="">Blog</a></p>
            </div>
            <div class="belanjaa">
                <h2>Download Aplikasi</h2>
                <a href=""><img src="<?php echo base_url('assets/klixaja/images/icons/google_play.png'); ?>" alt=""></a>
                <a href=""><img src="<?php echo base_url('assets/klixaja/images/icons/ios.png'); ?>" alt=""></a>
            </div>
            </div>
        </div>
        
        <!-- garis===================================================================== -->
        
      <div class="garissss" style="margin-top:70px"  ></div>

                <div class=" mt-5 d-sm-none d-md-block">
                        <div class="kolom22 mt-5">
                            <div class="pengiriman">
                            <h2>Pengiriman</h2>
                            <img src="<?php echo base_url('assets/klixaja/images/icons/icon_pengiriman.jpeg')?>" alt="">
                            </div>

                             <div class="pembayarann">
                            <h2>Pembayaran</h2>
                            <img src="<?php echo base_url('assets/klixaja/images/icons/icon_pembayaran.jpeg')?>" alt="">
                        </div>
                        </div>

                </div>

            <div class="garisss mt-2" ></div>

                <div class="padding_box_2 wh mt-3">
                    <div class="dis">
                        <p style="margin-left:30px;">&copy 2020 klixaja. All Rights Reserved</p>
                        <!-- <div class="iconv">
                            <p>We Using Safe Payment For</p>
                
                            <img src="assets/klixaja/images/icons/icon_bca.png";?>" alt="">
                            <img style="margin-left:18px;" src="./assets/klixaja/images/icons/icon_mandiri.gif";?>" alt="">
                        </div> -->
                    </div>
                </div>
