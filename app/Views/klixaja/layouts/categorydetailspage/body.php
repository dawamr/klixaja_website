<div class="container toppp" >
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" style="z-index: 0;">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                    <img class="d-block w-100 images-carousel" src="<?php echo base_url('assets/klixaja/images/item1.jpg');?>" alt="First slide">
                    </div>
                    <div class="carousel-item">
                    <img class="d-block w-100 images-carousel" src="<?php echo base_url('assets/klixaja/images/item2.jpg');?>" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                    <img class="d-block w-100 images-carousel" src="<?php echo base_url('assets/klixaja/images/siap_santap1.jpeg');?>" alt="Third slide">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>  
            
            <div class="container mt-4 ">
                  <div class="row">
                <div class="col-3 d-none d-lg-block d-xl-block">
                    <div class="row">
                        <img  style="width: 20px; height:20px" src="<?php echo base_url('assets/klixaja/images/icons/menu.svg');?>" alt="">
                        <p class="ml-2 font-weight-bold">Semua Kategori</p>
                    </div>
                <div id="kat"  class=" kat col filter-font">
                  <a href="" style="text-decoration:none"><h6>Kue Ulang Tahun</h6></a>
                  <a href="" style="text-decoration:none"><h6>Kue Basah</h6></a>
                  <a href="" style="text-decoration:none"><h6>Kue Lapis</h6></a>
                  <a href="" style="text-decoration:none"><h6>Kue Pandan</h6></a>
                  <a href="" style="text-decoration:none"><h6>Kue Pukis</h6></a>
                  <a href="" style="text-decoration:none"><h6>Kue Pukis</h6></a>
                  <a href="" style="text-decoration:none"><h6>Kue Pukis</h6></a>
                  <a href="" style="text-decoration:none"><h6>Kue Pukis</h6></a>
                  <a href="" style="text-decoration:none"><h6>Kue Pukis</h6></a>
                  <a href="" style="text-decoration:none"><h6>Kue Pukis</h6></a>
                  <a href="" style="text-decoration:none"><h6>Kue Pukis</h6></a>
                </div>
                <h6 id="a"onClick="slideDown('kat','a')"class="ml-3 mt-2" style="cursor:pointer" >Lainnya <i id="panah"i class="fas fa-arrow-circle-down"></i></h6>
                    <div class="row mt-4">
                        <img  style="width: 20px; height:20px" src="<?php echo base_url('assets/klixaja/images/icons/filter.png');?>" alt="">
                        <p class="ml-2 font-weight-bold ">Saring Filter</p>
                    </div>
                    <div class="row">
                        <p class="ml-2 font-weight-bold">Lokasi</p>
                    </div>
                    <div id="lok" onload="slideUp('lok')" class="col filter-font">
                          <h6> <input type="checkbox">
                        <span class="checkmark"></span> Jabodetabek</h6>
                          <h6> <input type="checkbox">
                        <span class="checkmark"></span>DKI Jakarta</h6>
                          <h6> <input type="checkbox">
                        <span class="checkmark"></span>Jawa Barat</h6>
                          <h6> <input type="checkbox">
                        <span class="checkmark"></span>Jawa Timur</h6>
                          <h6> <input type="checkbox">
                        <span class="checkmark"></span>Jawa Tengah</h6>
                          <h6> <input type="checkbox">
                        <span class="checkmark"></span>Kepulauan Riau</h6>
                          <h6> <input type="checkbox">
                        <span class="checkmark"></span>Banten</h6>
                          <h6> <input type="checkbox">
                        <span class="checkmark"></span>Sumatera Utara</h6>
                          <h6> <input type="checkbox">
                        <span class="checkmark"></span>Di Yogyakarta</h6>
                          <h6> <input type="checkbox">
                        <span class="checkmark"></span>Bali</h6>
                          <h6> <input type="checkbox">
                        <span class="checkmark"></span>Lampung</h6>
                          <h6> <input type="checkbox">
                        <span class="checkmark"></span>Sumatera Selatan</h6>

                    </div>
                    <h6 id="l"onClick="slideDown('lok','l')"class="ml-3 mt-2" style="cursor:pointer" >Lainnya <i id="panah" class="fas fa-arrow-circle-down"></i></h6>
                <div id="batasHarga"></div>
                    <div class="row" id="" >
                        <p class="ml-2 font-weight-bold">Batas Harga</p>
                    </div>
                    <div class="col">
                        <div class="row">
                              <input class="bentuk-input "min="0" type="number" placeholder="RP MIN"style="width:80px; height:32px;text-align:center;font-size:12px">
                              <div class="garis-input" style="height:1px; width:20px; background:#000;margin-top:15px;margin-left:5px;margin-right:5px;"></div>
                              <input class="bentuk-input " min="1" type="number" placeholder="RP MAX"style="width:80px; height:32px;text-align:center;font-size:12px">
                        </div>
                        <button type="button"class="btn btn-danger mt-3 ml-4 bentuk-input-btn text-center" style="text-align:center">Terapkan</button>
                     </div>
                     <div class="row mt-4">
                        <p class="ml-2 font-weight-bold ">Jenis produk</p>
                     </div>
                    <div class="col filter-font">
                         <h6> <input type="checkbox">
                        <span class="checkmark"></span>Vegan</h6>
                          <h6> <input type="checkbox">
                        <span class="checkmark"></span>Vegetarian</h6>
                    </div>


                      <div class="row mt-4">
                        <p class="ml-2 font-weight-bold">Penilaian</p>
                    </div>
                    <div class="col ">
                        <div class=" rating sized_box col_auto_max_content_5" style="margin-left:-16px">
                        <label class="parent star full">&starf;</label>
                        <label class="parent star full">&starf;</label>
                        <label class="parent star full">&starf;</label>
                        <label class="parent star full">&starf;</label>
                        <label class="parent star full">&starf;</label>
                      </div>
                    
                      <div class="row align-items-parent">
                     <div class=" rating sized_box col_auto_max_content_5">
                        <label class="parent star full">&starf;</label>
                        <label class="parent star full">&starf;</label>
                        <label class="parent star full">&starf;</label>
                        <label class="parent star full">&starf;</label>
                        <label class="parent star empty">&starf;</label>
                      </div>
                      <p style="margin-left:5px;margin-top:6px">ke atas</p>
                      </div>
                     
                      <div class="row">
                    <div class=" rating sized_box col_auto_max_content_5">
                        <label class="parent star full">&starf;</label>
                        <label class="parent star full">&starf;</label>
                        <label class="parent star full">&starf;</label>
                        <label class="parent star empty">&starf;</label>
                        <label class="parent star empty">&starf;</label>
                      </div>
                      <p style="margin-left:5px;margin-top:6px">ke atas</p>
                      </div>
                      
                      <div class="row">
                    <div class=" rating sized_box col_auto_max_content_5">
                        <label class="parent star full">&starf;</label>
                        <label class="parent star full">&starf;</label>
                        <label class="parent star empty">&starf;</label>
                        <label class="parent star empty">&starf;</label>
                        <label class="parent star empty">&starf;</label>
                      </div>
                      <p style="margin-left:5px;margin-top:6px">ke atas</p>
                    </div>
                        
                    <div class="row">
                        <div class=" rating sized_box col_auto_max_content_5">
                            <label class="parent star full">&starf;</label>
                            <label class="parent star empty">&starf;</label>
                            <label class="parent star empty">&starf;</label>
                            <label class="parent star empty">&starf;</label>
                            <label class="parent star empty">&starf;</label>
                        </div>
                      <p style="margin-left:5px;margin-top:6px">ke atas</p>
                    </div>
                    </div>
                     <div class="row">
                        <p class="ml-2 font-weight-bold">Opsi Pengiriman</p>
                    </div>
                    <div class="col filter-font">
                         <h6> <input type="checkbox">
                        <span class="checkmark"></span>J&T REG</h6>
                          <h6> <input type="checkbox">
                        <span class="checkmark"></span>JNE YES</h6>
                        <h6> <input type="checkbox">
                        <span class="checkmark"></span>JNE REG</h6>
                        
                    </div>

                   


                     
                </div>
                <div class="col-lg-9 col-xl-9 col-sm back " style="padding:0;margin:0" >
                    <div class="row align-items-center" >
                      <nav  class="navbar float:left" style="background:rgb(245, 243, 243);margin-top:auto;margin-bottom:auto;width:100%;height:54px;">
                        <ul id="urutkan"  class="navbarrr fonttt margin_auto q py-1" >
                             <h6 id="urut"class="fonttt d-none d-sm-block" style="float:left;margin-right:5px;margin-top:5px">Urutkan</h6>
                            <li class="x">
                              <a class="aktiff w" id="populer" href="#Populer">Populer</a>
                            </li>
                            <li class="x"><a class="w" id="terbaru" href="#Terbaru">Terbaru</a></li>
                            <li class="x"><a class="w" id="terlaris" href="#Terlaris">Terlaris</a></li>
                         <li class="x nav-item dropdown back-color mr-2">
                                    <a id="harga"class=" w nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Harga
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="#">Rendah ke Tinggi</a>
                                    <a class="dropdown-item" href="#">Tinggi ke Rendah</a>
                            </li>
                        

                            <li class="d-lg-none d-xl-none "data-toggle="modal" data-target="#exampleModalCenter"> 
                            <a href="#">
                              <div class="row" style="margin-left:0;margin-right:0">
                                <img  style="width: 15px; height:15px;" src="<?php echo base_url('assets/klixaja/images/icons/filter.png');?>" alt="">
                                Filter
                            </div>
                             </a>
                          </li>

                            <!-- Modal -->
                            <div class="modal fade modal-right" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                              <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Filter</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                     <div class="col-12">
                                     <p class="font-weight-bold">Lokasi</p>
                                      </div>
                                <div id="lokFilter" onload="slideUp('lokFilter')" class="klik">
                                  
                                      <div class="row bg-modal">
                                      
                                      <div class="col-6">
                                            <button type="button" class="btn  w-100">Jabodetabek</button>
                                      </div>
                                      <div class="col-6">
                                          <button type="button" class="btn  w-100">DKI Jakarta</button>
                                      </div>
                                      </div>

                                      <div class="row mt-2 bg-modal">
                                      <div class="col-6">
                                            <button type="button" class="btn  w-100">Jawa Tengah</button>
                                      </div>
                                      <div class="col-6">
                                          <button type="button" class="btn w-100">Jawa Barat</button>
                                      </div>
                                      </div>

                                      <!-- <button class="slidedown">dkfdfjkb,mbnuih  u</button> -->
                                      <div class="row mt-2 bg-modal">
                                      <div class="col-6 btnn">
                                            <button type="button" class="btn w-100">Jawa Timur</button>
                                      </div>
                                      <div class="col-6 btnn">
                                          <button type="button" class="btn  w-100">Sumatera Selatan</button>
                                      </div>
                                      </div>
                                      <div class="row mt-2 bg-modal">
                                        
                                      <div class="col-6 btnn">
                                            <button type="button" class="btn  w-100">Banten</button>
                                      </div>
                                      <div class="col-6 btnn">
                                          <button type="button" class="btn  w-100">Lampung</button>
                                      </div>
                                      </div>
                                </div>
                                <h6 id="d"onClick="slideDown('lokFilter','d')"class="ml-3 mt-2" style="cursor:pointer;text-align:center" >Lainnya <i class="fas fa-arrow-circle-down"></i></h6>
                                    <div class="garisss" style="margin-top: 20px;margin-bottom:10px"></div>

                                    <div class="klik">

                                    <div class="row bg-modal">
                                      <div class="col-12">
                                     <p class="font-weight-bold">Opsi Pengiriman</p>
                                      </div>
                                    <div class="col-6">
                                          <button type="button" class="btn  w-100">J&T REG</button>
                                    </div>
                                    <div class="col-6">
                                        <button type="button" class="btn  w-100">JNE REG</button>
                                    </div>
                                      <div class="col-6">
                                        <button type="button" class="btn mt-2 w-100">JNE YES</button>
                                    </div>
                                    </div>



                                   

                                   
                                  </div>
                                    <div class="garisss" style="margin-top: 20px;margin-bottom:10px"></div>
                                    
                                  
                                      <div class="row bg-modal">
                                      <div class="col-12">
                                       <p class="font-weight-bold">Batas Harga (Rp)</p>
                                       <div class="col">
                                         <div class="row">
                                           <div class="col ">
                                            <input class="lebar-input" type="number" placeholder="RP MIN"style=" height:32px;text-align:center;font-size:12px">

                                           </div>
                                           <div class="col-2">
                                            <div class="panjang" style="height:1px; width:20px; background:#000;margin-top:15px;margin-left:5px;margin-right:5px;"></div>

                                           </div>
                                           <div class="col">
                                            <input class="lebar-input" type="number" placeholder="RP MAX"style=" height:32px;text-align:center;font-size:12px">
                                           </div>
                                         </div>
                              
                                        </div>
                                       </div>
                                    </div>
                                <div id="klix" class="klix">
                                     <div class="row bg-modal">
                                      <div class="col">
                                        <button type="button" style="background: rgb(233, 232, 232);font-size: 12px;" class="btn mt-2  w-100 modal-color bg-modalll">0-75RB</button>
                                      </div>
                                        <div class="col">
                                        <button type="button"style="background: rgb(233, 232, 232);font-size: 12px;" class="btn mt-2 w-100  ">75RB-150RB</button>
                                      </div>
                                      <div class="col">
                                        <button type="button" style="background: rgb(233, 232, 232);font-size: 12px;" class="btn mt-2  w-100 ">150RB-200RB</button>
                                      </div>
                                   </div>     
                                 </div>
                                    <div class="garisss" style="margin-top: 20px;margin-bottom:10px"></div>

                                  <div class="klik">
                                     <div class="row bg-modal">
                                      <div class="col-12">
                                     <p class="font-weight-bold">Jenis Produk</p>
                                      </div>
                                    <div class="col-6">
                                          <button type="button" class="btn  w-100">Vegan</button>
                                    </div>
                                    <div class="col-6">
                                        <button type="button" class="btn  w-100">Vegetarian</button>
                                    </div>
                                    </div>
                                  </div>
                                    <div class="garisss" style="margin-top: 20px;margin-bottom:10px"></div>
                                  <div class="klik">
                                     <div class="row bg-modal">
                                      <div class="col-12">
                                     <p class="font-weight-bold">Penilaian</p>
                                      </div>
                                    <div class="col-6">
                                          <button type="button" class="btn  w-100">Bintang 5</button>
                                    </div>
                                    <div class="col-6">
                                        <button type="button" class="btn  w-100">Bintang 4 ke atas</button>
                                    </div>
                                    </div>
                                     <div class="row bg-modal">

                                    <div class="col-6">
                                          <button type="button" class="btn mt-2  w-100">Bintang 3 ke atas</button>
                                    </div>
                                    <div class="col-6">
                                        <button type="button" class="btn mt-2 w-100">Bintang 2 ke atas</button>
                                    </div>
                                    <div class="col-6">
                                        <button type="button" class="btn mt-2 w-100">Bintang 1 ke atas</button>
                                    </div>
</div>
                                    </div>


                                 
                                  
                                  </div>

                                  


                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Reset</button>
                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Terapkan</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                         
                        </ul>  
                      </nav>
                    </div>

                    
                   
            <div class="containn">
              <?php
              for($i=1;$i<41;$i++){
                  $rand = rand(1,5);
                  ?>
              <div >
              <div class="bentukk" style="position:relative"


              >
 <div  class="decoration_label_box  posisi_absolute" style=""><span>Star Seller</span></div>
              <div class="decoration_label_box decoration_label_box_2 posisi_absolute" style="right:0"><span>33%</span><span>Off</span></div>
               
              <a style="text-decoration:none" href="Details">
              <img src="<?php echo base_url('assets/klixaja/images/preorder3.jpg');?>" alt=""> 
              </a>
                  
              <!-- <div class="tombol" >
                  <div class="desain_button" >
                  <a href=""><button class="bg_button_addtocart"type="button">Add to cart</button></a>  <br>
                  <a href=""><button class="bg_button_quickview" type="button">Quick View</button></a> 
                  </div>
              </div> -->
              <a style="text-decoration:none" href="Details">

                <div class="max_line" >
              <p class="font_judull hide-text" style="font-size: 15px;">Martabak Spesial vegetarian asal palembang</p>
              </div> 
              </a>
              <div class="image_desc " style="margin-top:-20px;">
                      <div class="image_desc_detail">
                          <i class="heart_icon icon " title="Tambahkan ke wishlist"></i>
                        <a style="text-decoration:none" href="Details">
                          <div class="rating sized_box col_auto_max_content_5">
                              <label class="parent star full">&starf;</label>
                              <label class="parent star full">&starf;</label>
                              <label class="parent star full">&starf;</label>
                              <label class="parent star half">&starf;</label>
                              <label class="parent star empty">&starf;</label>
                          </div>
                        </a>
                      </div>
                   <a style="text-decoration:none" href="Details">

                     <div class="hargaa" style="">
                          <p class=" hrg decoration_sale" style="float:left;margin-right:5px;font-size:14px;margin-block-end:0px;margin-block-start:0px">Rp1.250.000</p>
                          <p class="hrgg decoration_discount" style="font-size:14px;margin:0;padding:0;margin-block-end:0px;margin-block-start:0px">Rp13.000.000</p>
                          <!-- grid,gap=10px,  grid-template-columns: max-content max-content; -->

                      </div>
                   </a>
                  </div>
                  
              </div>
              </div>
           <?php } ?>
          </div>

                  </div>
            </div>


          

        </div>
          <div id="gar" class="garisss" style="margin-top: 20px;"></div>

               


                     <div class="mt-5">
            <div class="info">
               <div class="belanjaa">
                   <h2>Kontak Kami</h2>
            <p><a href="" style="pointer-events: none;"><i class="fas fa-phone-square-alt"> 0851-2354-2121(08.00 - 18.00)</i></a></p>
            <p><a href="" style="text-decoration:underline;color:blue"><i style="color: #666"class="fas fa-envelope"></i> klix@gmail.com</a></p>
            
        </div>
<div class="belanjaa">
                <h2>Pembeli</h2>
                <p><a href="">Panduan Belanja</a></p>
                <p><a href="">Pembatalan dan Pengembalian</a></p>
            </div>
            <div class="belanjaa">
                <h2>Penjual</h2>
                <p><a href="">Alasan Jadi Penjual</a></p>
                <p><a href="">Cara Menjadi Penjual</a></p>
                <p><a href="">Ketentuan Produk</a></p>
                <p><a href="">Ketentuan Penjual</a></p>

            </div>

            <div class="belanjaa">
                <h2>Klixaja</h2>
                <p><a href="">Tentang Kami</a></p>
                <p><a href="">Visi & Misi</a></p>
                <p><a href="">Syarat & Ketentuan</a></p>
                <p><a href="">FAQs</a></p>
                <p><a href="">Blog</a></p>
            </div>
            <div class="belanjaa">
                <h2>Download Aplikasi</h2>
                <a href=""><img src="<?php echo base_url('assets/klixaja/images/icons/google_play.png'); ?>" alt=""></a>
                <a href=""><img src="<?php echo base_url('assets/klixaja/images/icons/ios.png'); ?>" alt=""></a>
            </div>
            </div>
        </div>
        
        <!-- garis===================================================================== -->
        
      <div class="garissss" style="margin-top:70px" ></div>
                <div class=" mt-5 d-sm-none d-md-block">
                        <div class="kolom22 mt-5">
                            <div class="pengiriman">
                            <h2>Pengiriman</h2>
                            <img src="<?php echo base_url('assets/klixaja/images/icons/icon_pengiriman.jpeg');?>" alt="">
                            </div>

                             <div class="pembayarann">
                            <h2>Pembayaran</h2>
                            <img src="<?php echo base_url('assets/klixaja/images/icons/icon_pembayaran.jpeg');?>" alt="">
                        </div>
                        </div>
                </div>
            <div class="garisss mt-2" ></div>

                <div class="padding_box_2wh mt-3">
                    <div class="dis">
                        <p style="margin-left:30px;">&copy 2020 klixaja. All Rights Reserved</p>

                        <!-- <div class="iconv">
                            <p>We Using Safe Payment For</p>
                
                            <img src="assets/klixaja/images/icons/icon_bca.png";?>" alt="">
                            <img style="margin-left:18px;" src="assets/klixaja/images/icons/icon_mandiri.gif";?>" alt="">
                        </div> -->
                    </div>

      
                </div>
               