<div class="content">
<header class="header_navbar">
    <div class="navbar_list">
        <div class="navbar_top">
            <div class="navbar_top_row1">
                <a href="#" class="navbar_top_row1_link">Jual</a>
                <a href="#" class="navbar_top_row1_link">Download</a>
                <div class="navbar_top_row1_follow_us">
                    <h4 style="margin-top:5px;" class="navbar_top_row1_follow_us_title">Ikuti Kami</h4>
                    <a href="#" class="navbar_top_row1_follow_us_link">
                        <i class="ig_icon icon navbar_top_icon"></i>
                    </a>
                    <a href="#" class="navbar_top_row1_follow_us_link">
                        <i class="fb_icon icon navbar_top_icon"></i>
                    </a>
                    <!-- <a href="#" class="navbar_top_row1_follow_us_link">
                        <i class="twitter_icon icon navbar_top_icon"></i>
                    </a>
                    <a href="#" class="navbar_top_row1_follow_us_link">
                        <i class="line_icon icon navbar_top_icon"></i>
                    </a> -->
                    <a href="#" class="navbar_top_row1_follow_us_link">
                        <i class="wa_icon icon navbar_top_icon"></i>
                    </a>
                </div>
            </div>
            <div class="navbar_top_row3">
                <a href="#" class="navbar_top_row3_link">
                    <i class="help_icon icon navbar_top_icon"></i>
                    <span>Bantuan</span>
                </a>
            </div>
        </div>
        <!-- end navbar_top -->
        <div class="navbar_bottom">
            <a href="#" class="nav_icon">
                <svg width="30" height="25">
                    <path d="M0,5 30,5" stroke="#fff" stroke-width="5" />
                    <path d="M0,14 30,14" stroke="#fff" stroke-width="5" />
                    <path d="M0,23 30,23" stroke="#fff" stroke-width="5" />
                </svg>
            </a>
            
            <div class="logo">
                <a href="#" class="logo_link">
                    <div class="logo_image" style="display: inline-block;"></div>
                </a>   

            </div>
             
            <div class="search_bar">
                <input type="text" id="search" placeholder="Apa yang ingin anda cari?" onclick="this.placeholder=''"
                       onblur="this.placeholder='Apa yang ingin anda cari?'" class="search_field" />
                <div class="search_icon_box">
                    <i class="search_icon icon"></i>
                </div>
            </div>
             <div class="cart" onclick="window.location.href='Cart';">
                <i class="cart_icon icon"></i>
                <div class="cart_quantity">6</div>
                <div class="cart_isosceles" style="display:none"></div>
                <div class="cart_content" style="display:none">
                    <!-- <p class="cart_no_product">Tidak ada produk.</p> -->
                    <?php for ($i = 0; $i < 3; $i++) { ?>
                        <div class="cart_item">
                            <img src="<?php echo base_url('assets/klixaja/images/item1.jpg'); ?>" class="cart_item_image">
                            <p class="cart_item_title">
                                <?php echo substr("Laborum ex fugiat cillum mollit tempor laborum excepteur do adipisicing voluptate.", 0, 50). "..."; ?>
                            </p>
                            <p class="cart_item_price"><?php echo substr("Rp150.000.000.000", 0, 13). "..."; ?></p>
                            <a href="#" class="cart_item_delete"></a>
                        </div>
                    <?php } ?>
                    <a href="Cart" class="cart_button">Lihat Keranjang</a>
                </div>
            </div>
            <div class="notifications">
                <i class="bell_icon icon"></i>
            </div>
            <div class="user ">
                <i class="user_icon icon  "id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
                 <div class="dropdown-menu profile-dropdown" style="width:400px" aria-labelledby="dropdownMenuButton">
                 <div style="display:flex;align-items:center;margin-left:20px;cursor:pointer">
                 <img style="width:50px;height:50px;border-radius:50%;margin-right:10px" src="assets/klixaja/images/item3.jpg" alt="">
                 <p style="font-weight:bold;font-size:18px">Indo Shop</p>
                </div>
                <hr style="margin-bottom:-3px">
    <a class="dropdown-item" style="height:40px;font-size:16px;display:flex;align-items:center" href="#">Pesanan Saya</a>
        <a class="dropdown-item" style="height:40px;font-size:16px;display:flex;align-items:center" href="#">History Pembelian</a>
                <a class="dropdown-item" style="height:40px;font-size:16px;display:flex;align-items:center" href="#">TopUp Saldo</a>
    <a class="dropdown-item" style="height:40px;font-size:16px;display:flex;align-items:center" href="#">Registrasi Mitra</a>
    <a class="dropdown-item" style="height:40px;font-size:16px;display:flex;align-items:center" href="#">Edit Profile</a>
    <a class="dropdown-item" style="height:40px;font-size:16px;display:flex;align-items:center" href="#">Pengaturan</a>
    <a class="dropdown-item" style="height:40px;font-size:16px;display:flex;align-items:center" href="#">Help Center</a>
        <a class="dropdown-item" style="height:40px;font-size:16px;display:flex;align-items:center" href="#">Logout</a>



    
  </div>
            </div>

 

        </div>
        <!-- end navbar_bottom -->
    </div>
    <!-- end navbar_list -->
</header>
